# delete-all-dbs.sh script
#
# @author Sonja Happ <sonja.happ@eonerc.rwth-aachen.de>, Jonas Otten
# @copyright 2019, Institute for Automation of Complex Power Systems, EONERC
# @license GNU General Public License (version 3)
#
# DistAIX Web
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
################################################################################

# Postgres

dbname="swarmgrid"
postgresUser="distaix"
postgresTables=( "agentmetadata" "resulttypes" "simulationmetadata" "timemeasurements" "timemeasurementtypes" "simulations" )
postgresHost="ubuntu@postgres.acs-distaix.osc.eonerc.rwth-aachen.de"
cassandraHost="ubuntu@cassandra01.acs-distaix.osc.eonerc.rwth-aachen.de"

for table in "${postgresTables[@]}"
do
	echo "[DELETING] $table"
	ssh ${postgresHost} psql -U ${postgresUser} -h 127.0.0.1 -d ${dbname} -c \"TRUNCATE $table CASCADE\"
	echo ""
done

# Cassandra
echo "[DELETING] Cassandra Table results"
ssh ${cassandraHost} "cqlsh -e 'TRUNCATE TABLE ${dbname}.results'"
