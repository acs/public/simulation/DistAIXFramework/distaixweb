var path = require('path');
var webpack = require('webpack');

module.exports = {
	mode: "production",
	entry: "./app/app.js",
	output:{
		filename: 'bundle.js',
		path: path.resolve(__dirname, 'public/dist'),
		publicPath: "/dist"
	},
	module: {
		rules: [
			{ test: /\.jsx?$/, exclude: /node_modules/, loader:"babel-loader"},
			{ test: /\.scss$/, exclude: /node_modules/, loader:"style-loader"},
			{ test: /\.scss$/, exclude: /node_modules/, loader:"css-loader"},
			{ test: /\.scss$/, exclude: /node_modules/, loader:"sass-loader"}
  		]
	},
	plugins:[
		new webpack.DefinePlugin({ // <-- key to reducing React's size
			'process.env': {
				'NODE_ENV': JSON.stringify('production')
      			}
    		}),
		//new webpack.optimize.UglifyJsPlugin(), //minify everything
		new webpack.optimize.AggressiveMergingPlugin(), //Merge chunks
		new webpack.HotModuleReplacementPlugin(),
		new webpack.NoEmitOnErrorsPlugin()
	],

	devServer:{
		static: {
			directory: path.join(__dirname, 'public')
		},
		port: 8080
	}
};
