// App class (main)
//
// @author Sonja Happ <sonja.happ@eonerc.rwth-aachen.de>, Jonas Otten
// @copyright 2019, Institute for Automation of Complex Power Systems, EONERC
// @license GNU General Public License (version 3)
//
// DistAIX Web
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

import React from 'react';
import {render} from 'react-dom';
import './css/app.scss';
import NodeStatus from './components/NodeStatus.js';
import DistAIXSidebar from './components/DistAIXSidebar.js';
import GraphCreator from './components/creator/GraphCreator.js';
import Graph        from './components/GraphView.js';
import DeleteGraph  from './components/DeleteGraph.js';
import DeleteSimulation  from './components/DeleteSimulation.js';

import PreparedResults  from './components/PreparedResults.js';
import Requests from './components/requests/requests.js';

const COMPONENTS={
  DELETEGRAPH: 1,
  DELETESIMULATION: 2,
  NEWGRAPH: 3,
  NODESTATUS: 4,
  PREPAREDRESULTS: 5,
  SHOWGRAPH: 6
};

class App extends React.Component{
  constructor(props) {
    super(props)
    this.state = {graphs:[], activeComponent: COMPONENTS.NODESTATUS, activeGraph: {}, errorMessage: "", hasError: false};
  }
  componentDidMount(){
    this.loadSidebar();
  }
  loadSidebar(){
    let r = this;
    Requests.get("/api/v1/sidebar",(res, err) => { //load sidebar data
      if(err){
        r.onError("Could not get sidebar elements: ["+err.status+ "] "+err.message);
      }else{
        if (!Array.isArray(res)){
          r.onError("Response (sidebar) received is not an array: " + res.toString())
        } else {
          r.setState({graphs:res});
        }

      }
    });
  }
  onGraphCreated(g){ //if a graph get's created by the setup, this method get's called
    let r = this;
    let graphs = this.state.graphs;
    Requests.post("/api/v1/sidebar", g, (res, err) =>{
      if(err){
        r.onError("Could not post new graph: ["+err.status+ "] "+err.message);
      }else{
        r.setState({hasError: false});
        graphs.push(g); //if the creation is sucessfull, add it to the sidebar
        r.setState({graphs: graphs});
      }
    });
  }
  onActiveComponentChange(id){
    this.setState({activeComponent: id, hasError: false});
  }
  onGraphSelected(el){
    this.setState({activeGraph: el, activeComponent: COMPONENTS.SHOWGRAPH, hasError: false});
  }
  onDelete(sidebarData){
    this.setState({graphs: sidebarData});
  }
  onError(message){
    this.setState({hasError: true, errorMessage: message});
  }
  getActiveComponent(){
    switch(this.state.activeComponent){
      case COMPONENTS.NODESTATUS:
        return <NodeStatus onError={this.onError.bind(this)} />
      case COMPONENTS.NEWGRAPH:
        return <GraphCreator onGraphCreated={this.onGraphCreated.bind(this)} onError={this.onError.bind(this)} />
      case COMPONENTS.DELETEGRAPH:
        return <DeleteGraph graphs={this.state.graphs} onDelete={this.onDelete.bind(this)} onError={this.onError.bind(this)} />
      case COMPONENTS.DELETESIMULATION:
        return <DeleteSimulation graphs={this.state.graphs} onDelete={this.onDelete.bind(this)} onError={this.onError.bind(this)} />
      case COMPONENTS.PREPAREDRESULTS:
        return <PreparedResults onError={this.onError.bind(this)} />
      case COMPONENTS.SHOWGRAPH:
        return <Graph graph={this.state.activeGraph} onError={this.onError.bind(this)} />
      default:
        console.log("No compononet foud with ID "+this.state.activeComponent);
        return <div />
    }
  }
  onErrorDismiss(){
    this.setState({hasError: false});
  }
  render(){
    let activeComponent = this.getActiveComponent();
    let error;
    if(this.state.hasError){
      error = <div id="error" onClick={this.onErrorDismiss.bind(this)}>{this.state.errorMessage}</div>
    }
    return(
	    <div id="appMainContainer">
	      <div id="appNav">
		      <DistAIXSidebar
                  sidebarElements={this.state.graphs}
                  onError={this.onError.bind(this)}
                  COMPONENTS={COMPONENTS}
                  setComponent={this.onActiveComponentChange.bind(this)}
                  graphSelected={this.onGraphSelected.bind(this)}
              />
	       </div>
         <div id="mainErrorWrapper">
          {error}
          {activeComponent}
        </div>
	    </div>
    );
  }
}

render(<App />, document.getElementById("distaix-web-container"));
