// GraphControl class
//
// @author Sonja Happ <sonja.happ@eonerc.rwth-aachen.de>, Jonas Otten
// @copyright 2019, Institute for Automation of Complex Power Systems, EONERC
// @license GNU General Public License (version 3)
//
// DistAIX Web
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////
import React from "react";


// GraphControl capsulates the control buttons/inputs to manipulate or
// update the data which is shown
class GraphControl extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            steps: false,
            start: this.props.start,
            stop: this.props.stop,
            info: "",
            selectedInfo: ""
        };
    }

    componentWillReceiveProps(props) {
        if ("Simulation ID" in props.information) {
            this.setState({ info: props.information["Simulation ID"], selectedInfo: "Simulation ID" });
        }
        //check if sth changed
        if (this.state.start !== props.start || this.state.stop !== props.stop) {
            this.setState({ start: props.start, stop: props.stop });
        }
    }


    updateGraph() {
        this.props.updateGraph();
    }
    downloadCSV() {
        this.props.downloadCSV()
    }
    newStartValue(ev) {
        let x = parseFloat(ev.target.value)
        this.props.onStartUpdate(x);
        this.setState({ start: x });
    }
    newStopValue(ev) {
        let x = parseFloat(ev.target.value)
        this.props.onStopUpdate(x);
        this.setState({ stop: x });
    }
    showInfo(el) {
        let k = el.target.value;
        this.setState({ info: this.props.information[k], selectedInfo: k });
    }
    render() {
        let dropdown = Object.keys(this.props.information).map((el, i) => {
            if (el === this.state.selectedInfo) {
                return <option value={el} defaultValue="selected">{el}</option>
            }
            return <option value={el}>{el}</option>
        });
        // needed to determine graph size

        let infoStyle = {};
        if (this.props.infoActive) {
            infoStyle = { backgroundColor: "teal" };
        }

        let startInput = <input type="number" name="fstart" min="0" value={this.state.start} onChange={this.newStartValue.bind(this)} />
        if(this.state.start === -1){
            startInput = <input type="number" name="fstart" min="0" onChange={this.newStartValue.bind(this)} />
        }
        let stopInput = <input type="number" name="fstop" min="0" value={this.state.stop} onChange={this.newStopValue.bind(this)} />
        if(this.state.stop === -1){
            stopInput = <input type="number" name="fstop" min="0" onChange={this.newStopValue.bind(this)} />
        }

        return (<div id="GraphControl" >
            <div id="GraphHeader">{this.props.title}</div>
            <div id="GraphControlSettings">
                <div style={{ display: "flex", alignItems: "center", paddingLeft: "25px" }}>
                    <div>Start</div> {startInput}
                </div>
                <div id="GraphControlUpdate" onClick={this.updateGraph.bind(this)}>
                    Update
                </div>
                <div id="GraphControlDownloadCSV" onClick={this.downloadCSV.bind(this)}>
                    Download CSV
                </div>
                <div id="GraphControlShowInformations" style={infoStyle} >
                    <select onChange={this.showInfo.bind(this)}>{dropdown}</select>
                    <div style={{ textAlign: "center", padding: "5px" }} >{this.state.info}</div>
                </div>
                <div style={{ display: "flex", alignItems: "center" }}>
                    <div>Stop</div> {stopInput}
                </div>
            </div>
        </div>);
    }
}

export default GraphControl;