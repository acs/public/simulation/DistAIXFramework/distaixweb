// GraphView class
//
// @author Sonja Happ <sonja.happ@eonerc.rwth-aachen.de>, Jonas Otten
// @copyright 2019, Institute for Automation of Complex Power Systems, EONERC
// @license GNU General Public License (version 3)
//
// DistAIX Web
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

import React from 'react';
import DistAIXGraph from './DistAIXGraph.js';
import GraphControl from "./GraphControl";
import loadData from './requests/loadData.js';
import Requests from './requests/requests.js';

import '../css/components/GraphView.scss';

// The GraphView component is responsible for loading data and displaying it after
// a user clicked on a graph in the sidebar.
class GraphView extends React.Component {
	constructor(props) {
		super(props);
		// data: actual data points from backend
		// rightKeys/leftKeys: resulttypes shown on the right/left y-axis
		// start/stop: selected start/stop times, automatically set on request response
		this.state = {
			data: [],
			rightKeys: [],
			leftKeys: [],
			start: -1,
			stop: -1,
			simulation: 1,
			agents: "",
			right: "",
			left: "",
			information: {}
		};
	}
	componentDidMount() {
		this.loadMetadata();
		this.loadGraphdata(-1, -1, this.props);
	}

	componentWillReceiveProps(nextProps) {
		this.loadMetadata();
		this.loadGraphdata(-1, -1, nextProps);
	}

	loadGraphdata(setStart, setStop, props) {
		this.setLoading(true);
		loadData.withQueries(
			props.graph.simid,
			setStart,
			setStop,
			props.graph.agentids,
			props.graph.resulttypes_right,
			props.graph.resulttypes_left,
			(elements, err) => {
			this.setLoading(false);
			if(err){
				this.props.onError("Could not load data ["+err.status+"] "+err.message);
				return;
			}
			let start = elements.data[0].simTime;
			let stop = elements.data[elements.data.length - 1].simTime;
			this.setState({
				simulation: parseInt(props.graph.simid),
				data: elements.data,
				leftKeys: elements.leftKeys,
				rightKeys: elements.rightKeys,
				agents: props.graph.agentids,
				right: props.graph.resulttypes_right,
				left: props.graph.resulttypes_left,
				start: start,
				stop: stop
			});
		});
	}
	loadMetadata() {
		let r = this;
		Requests.get("/api/v1/simulations", (simulations, err) => {
			if(err){
				this.props.onError(err);
				return;
			}
			if (!Array.isArray(simulations)) {
				this.props.onError({status: "", message: "simulations are not an array!"});
				return;
			}
			let simulation = simulations.filter((el) => {
				return el.simulationid === r.props.graph.simid;
			});
			let information = {};
			information["Simulation ID"] = this.props.graph.simid;
			simulation[0].metadata.forEach((el) => {
				let key = el.metatype + "[" + el.id + "] " + el.key;
				if (el.id === -1) {
					key = el.metatype;
				}
				information[key] = el.value;
			});
			r.setState({ information: information });
		});
	}
	downloadCSV() {
		console.log("Download");
		window.location = "/api/v1/simulations/" + this.state.simulation + "/results/filtered/results.csv?" + loadData.getQueries(this.state.start, this.state.stop, this.state.agents, this.state.left, this.state.right);
	}
	setLoading(status) {
		if (document.getElementById('loadingOverlay') !== null) {
			document.getElementById('loadingOverlay').style.display = status ? "flex" : "none";
		}
	}
	onStartUpdate(newStart) {
		this.setState({ start: newStart });
	}
	onStopUpdate(newStop) {
		this.setState({ stop: newStop });
	}
	update() {
		this.loadGraphdata(this.state.start, this.state.stop, this.props);
	}
	render() {
		let graphctl = <GraphControl information={this.state.information} start={this.state.start} stop={this.state.stop} title={this.props.graph.url} onStartUpdate={this.onStartUpdate.bind(this)} onStopUpdate={this.onStopUpdate.bind(this)} updateGraph={this.update.bind(this)} downloadCSV={this.downloadCSV.bind(this)} />
		return (
			<div id="graphColumn">
				<div id="loadingOverlay">
					<div id="loadingOverlayVertical">
						<i className="fa fa-circle-o-notch fa-spin fa-3x fa-fw"/>
						<span className="sr-only">Loading...</span>
					</div>
				</div>
				{graphctl}
				<DistAIXGraph data={this.state.data} simulation={this.state.simulation} agents={this.state.agents} leftKeys={this.state.leftKeys} rightKeys={this.state.rightKeys} />
			</div>
		)
	}
}



export default GraphView;
