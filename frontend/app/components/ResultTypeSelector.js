// ResultTypeSelector class
//
// @author Sonja Happ <sonja.happ@eonerc.rwth-aachen.de>, Jonas Otten
// @copyright 2019, Institute for Automation of Complex Power Systems, EONERC
// @license GNU General Public License (version 3)
//
// DistAIX Web
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

import React from "react";
import Requests from "./requests/requests";

class ResultTypeSelector extends React.Component{
    constructor(props){
        super(props);
        this.state = {dataForSim: -1, data: [], selectedPairs: [], selectedRows : []};
    }
    loadElements(){
        this.setState({dataForSim: this.props.simulationID});
        //this.props.setLoading(true);
        //load agenttypes
        Requests.get('/api/v1/simulations/'+this.props.simulationID+'/agenttypes', (res, err)=>{
            if(err){
                this.props.onError(err);
                return;
            } else if (!Array.isArray(res)) {
                this.props.onError({status:"", message: "agenttypes is not an array"})
                return;
            }
            this.setState({data: res, selectedPairs: [], selectedRows: []});
        });

        //load simulation start and stop
        Requests.get('/api/v1/simulations/'+this.props.simulationID+'/times', (res, err)=>{
            if(err){
                this.props.onError(err);
                return;
            }
            this.props.onUpdateTimes(res.starttime, res.stoptime);
        });
    }
    selectedType(agenttypeid, resulttype, row){
        let pairs = this.state.selectedPairs
        let pair = {agenttypeid: agenttypeid, resulttype: resulttype};
        let index = -1;
        pairs.forEach((el, i)=>{
            if(el.agenttypeid === agenttypeid && el.resulttype === resulttype)
                index = i;
        });
        let selectedRows = this.state.selectedRows
        if(index !== -1){
            pairs.splice(index, 1);

            let rowIndex = selectedRows.indexOf(row);
            if(rowIndex !== -1){
                selectedRows.splice(rowIndex, 1);
            }
        }else{
            pairs.push(pair);
            selectedRows.push(row);
        }
        this.props.update(pairs);
        this.setState({selectedPairs: pairs, selectedRows: selectedRows});
    }
    render(){
        let header = "Result Types"
        if(this.state.dataForSim !== -1)
            header += " [for Simulation "+this.state.dataForSim+"]";

        if(this.props.simulationID === -1 && this.state.data.length === 0){ //return just a header if there's no valid sim id
            return(
                <div className="selectorColumn">
                    <div className="selectorColumnHeader">{header}</div>
                </div>
            )
        }
        let button;
        if(this.props.simulationID !== -1)
            button = <div onClick={this.loadElements.bind(this)} className="LoadDataButton">Load Resulttypes for Simulation {this.props.simulationID} </div>;

        let elements = [];
        let row = 0;
        this.state.data.forEach((el)=>{
            el.resulttypes.forEach((r)=>{
                let style = {}
                if(this.state.selectedRows.indexOf(row) !== -1){
                    style = {backgroundColor: 'crimson'};
                }
                elements.push(<div style={style} className="columnrow" onClick={this.selectedType.bind(this, el.id, r, row)}>{el.type} {r}</div>)
                row++;
            });
        });
        return(
            <div className="selectorColumn">
                <div className="selectorColumnHeader">{header}</div>
                {button}
                <div className="selectorColumnElements">{elements}</div>
            </div>
        )
    }
}

export default ResultTypeSelector