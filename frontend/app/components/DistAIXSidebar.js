// DistAIXSidebar class
//
// @author Sonja Happ <sonja.happ@eonerc.rwth-aachen.de>, Jonas Otten
// @copyright 2019, Institute for Automation of Complex Power Systems, EONERC
// @license GNU General Public License (version 3)
//
// DistAIX Web
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

import React from 'react';
import '../css/components/DistAIXSidebar.scss';

const sidebarborderstyle= {
	marginBottom: '10px',
	marginTop: '20px'
}


class DistAIXSidebar extends React.Component{
	constructor(props){
		super(props);
		this.state={sidebarElements:[]};
	}
	onClick(id){
		this.props.setComponent(id);
	}
	elementSelected(el){
		this.props.graphSelected(el);
	}
	render(){
		let sidebarContent = [];

		if(this.props.sidebarElements !== undefined ) {
			sidebarContent = this.props.sidebarElements.map((el,i)=>{
				return (<div key={i} className="sidebarElement" onClick={this.elementSelected.bind(this,el)}>{el.name}</div>);
			});
		}
		return(<div>
				<div id="sidebarHeadline">DistAIX Web</div>
				<div id="sidebarHeaderImgDiv">
				<img src="/static/logo_small.png" id="sidebarHeaderImg" alt={"DistAIX Logo"}/>
				</div>
				<div className="sidebarButton" onClick={this.onClick.bind(this, this.props.COMPONENTS.NEWGRAPH)}>New Graph</div>
				<div className="sidebarButton" onClick={this.onClick.bind(this, this.props.COMPONENTS.PREPAREDRESULTS)}>Prepared Results</div>
				<div className="sidebarButton" onClick={this.onClick.bind(this, this.props.COMPONENTS.DELETEGRAPH)}>Delete Graph</div>
				<div className="sidebarButton" onClick={this.onClick.bind(this, this.props.COMPONENTS.DELETESIMULATION)}>Delete Simulation</div>
				<div className="sidebarBorder" style={sidebarborderstyle}/>
				<div id="sidebarBorderElements">
				{sidebarContent}
				</div>
			</div>
		)
	}
}

export default DistAIXSidebar
