// DeleteSimulation class
//
// @author Sonja Happ <sonja.happ@eonerc.rwth-aachen.de>, Jonas Otten
// @copyright 2019, Institute for Automation of Complex Power Systems, EONERC
// @license GNU General Public License (version 3)
//
// DistAIX Web
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

import Requests from "./requests/requests.js";
import React from 'react';

import "../css/components/DeleteSimulation.scss";

class DeleteSimulation extends React.Component{
    constructor(props){
        super(props);
        this.state= {simids: [], selectedIDs: []};
    }
    componentDidMount(){
        this.load();
    }
    onclick(el,i){
        //either add the simid to the selectedIDs array or delete it if it's alreay in there
        let selectedIDs = this.state.selectedIDs;
        let index = selectedIDs.indexOf(el);
        if(index === -1){
            selectedIDs.push(el);
        }else{
            selectedIDs.splice(index, 1);
        }
        this.setState({selectedIDs: selectedIDs});
    }
    load(){
        let r = this;
        Requests.get("/api/v1/simulations",(res, err)=>{
            if(err){
                this.props.onError(err.message);
                return;
            }else if (!Array.isArray(res)){
                this.props.onError("Delete Simulation: simulations is not an array")
                return;
            }

            let simids = []
            if (res !== undefined){
                simids = res.map((simulation)=>{
                    return simulation.simulationid;
                });
            }
            r.setState({simids: simids});
        });
    }
    onDelete(){
        let r = this;
        //iterate over all selected IDs and make a DELETE to the corresponding endpoint, update the state afterwards
        this.state.selectedIDs.forEach((id)=>{
            Requests.delete("/api/v1/simulations/"+id,(res, err)=>{
                //r.props.setLoading(false);
			    if(err){
				    r.props.onError("Could not delete simulation ["+err.status+"] "+err.message);
				    return;
			    }
                //remove simid from simids and selecteIDs array
                let simids = r.state.simids;
                let selectedIDs = r.state.selectedIDs;
                let i_simids = simids.indexOf(id);
                let i_selectedIDs = selectedIDs.indexOf(id);
                simids.splice(i_simids, 1);
                selectedIDs.splice(i_selectedIDs, 1);

                //removed graphs from sidebar
                let newSidebar = r.props.graphs.filter( el => el.simid !== id);
                //propagate it to the parent object, so it can update the sidebar
                r.props.onDelete(newSidebar);

                r.setState({simids: simids, selectedIDs: selectedIDs});
                console.log("deleted "+id);
            });
        })
    }
    render(){
        let elements = this.state.simids.map((el)=>{
            let style={}
            if(this.state.selectedIDs.includes(el)){
                style.backgroundColor = "crimson"
            }
            return(
                <div className="deleteSimulationSimulationID" onClick={this.onclick.bind(this, el)} style={style}>{el}</div>
            )
        });
        let Button = <div id="SimulationDeleteButton" onClick={this.onDelete.bind(this)} style={{cursor: "pointer"}}>Delete</div>;
        if(this.state.selectedIDs.length === 0){
            Button = <div id="SimulationDeleteButton" style={{opacity: 0.4, cursor:"default"}} onClick={this.onDelete.bind(this)}>Delete</div>;
        }
		return(
            <div id="SimulationDelete">
                <div id="SimulationDeleteHeader">
                    <div style={{padding: "5px"}}>Please select an item to delete</div>
                    <div style={{display: "flex",  justifyContent: "space-around"}}><div style={{padding: "5px", backgroundColor: "red", borderRadius: "5px"}}>WARNING: This cannot be undone!</div></div>
                </div>
                <div id="deleteSimulationSimulations">{elements}</div>
                <div id="SimulationDeleteConfirm">{Button}</div>
            </div>
        )
    }
}

export default DeleteSimulation;