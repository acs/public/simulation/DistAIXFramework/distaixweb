// createURL function
//
// @author Sonja Happ <sonja.happ@eonerc.rwth-aachen.de>, Jonas Otten
// @copyright 2019, Institute for Automation of Complex Power Systems, EONERC
// @license GNU General Public License (version 3)
//
// DistAIX Web
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

export default function(simulation, left, right, agents, start, stop){
    let resultTypes  = createResultTypes(left);
    let resultsRight = createResultTypes(right);
    //create string for request
    if(resultTypes === ""){
        resultTypes+=resultsRight;
    }else{
        if(resultsRight !== "")
            resultTypes+=","+resultsRight;
    }
    let agentIDs = createAgentIDs(agents);

    let url = "/api/v1/simulations/"+simulation+"/results/filtered?resulttypes="
        +resultTypes+"&agents="+agentIDs;
    if(start !== -1 && stop !== -1){
        url+="&start="+start+"&stop="+stop;
    }
    return url;
}

//create resulttypes string for API parameter
function createResultTypes(arr){
    return arr.reduce((acc, val,i)=>{
        if(i===0){
            return acc+val.agentTypeID+"|"+val.name;
        }
        return acc+","+val.agentTypeID+"|"+val.name;
    },"");
}
//create agents string for API parameter
function createAgentIDs(arr){
    return arr.reduce((acc, val,i)=>{
        if(i===0){
            return acc+val.agentID
        }
        return acc+","+val.agentID;
    },"");
}