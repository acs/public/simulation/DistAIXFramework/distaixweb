// loadData function
//
// @author Sonja Happ <sonja.happ@eonerc.rwth-aachen.de>, Jonas Otten
// @copyright 2019, Institute for Automation of Complex Power Systems, EONERC
// @license GNU General Public License (version 3)
//
// DistAIX Web
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

import Requests from '../requests/requests.js';
import createURL from "./createURL.js";
import createURLQuery from './createURLQuery.js';

//get the data
export default {
    withTypes: function(simulation, start, stop, agents, right, left, callback){
        let url = createURL(simulation,left, right, agents, start, stop);
        makeRequest(url, left, right, callback);
    },
    withQueries(simulation, start, stop, agents, resulttypes_left, resulttypes_right, callback){
        let resulttypes = createURLQuery.appendResulttypes(resulttypes_left, resulttypes_right);
        let url = "/api/v1/simulations/"+simulation+"/results/filtered?start="+start+"&stop="
            +stop+"&resulttypes="+resulttypes+"&agents="+agents;
        let left = createResulttypeArray(resulttypes_left);
        let right = createResulttypeArray(resulttypes_right);
        makeRequest(url, left, right, callback)
    },
    getQueries(start, stop, agents, resulttypes_left, resulttypes_right){
        let resulttypes = createURLQuery.appendResulttypes(resulttypes_left, resulttypes_right);
        return "start="+start+"&stop="+stop+"&resulttypes="+resulttypes+"&agents="+agents;
    }
}

function createResulttypeArray(resulttypesQuery){
    let arr = resulttypesQuery.split(",");
    return arr.map((el) => {
        let typeArr = el.split("|");
        return {name: typeArr[1], agentTypeID: parseInt(typeArr[0])};
    });
}

function makeRequest(url, left, right, callback){
    let rightKeys = [];
    let leftKeys  = [];

    Requests.get(url, (res, err)=>{
        if(err){
            return callback(undefined, err)
        }
        let data = res.map((el)=>{
            let ret = {simTime: el.simTime};
            let arr = el.values.forEach((v)=>{
                let key = v.agentID+" "+v.agentTypeName+" "+v.resultTypeName;
                ret[key] = v.value;
                //add keys to left and right
                left.forEach((el)=>{
                    if(el.name === v.resultTypeName && el.agentTypeID === v.agentTypeID && leftKeys.indexOf(key) === -1){
                        leftKeys.push(key);
                    }
                });
                right.forEach((el)=>{
                    if(el.name === v.resultTypeName && el.agentTypeID === v.agentTypeID && rightKeys.indexOf(key) === -1){
                        rightKeys.push(key);                        
                    }
                });
            });
            return ret;
        });
        callback({data:data, rightKeys: rightKeys, leftKeys: leftKeys}, undefined);
    });
}