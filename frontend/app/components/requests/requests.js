// Requests functions
//
// @author Sonja Happ <sonja.happ@eonerc.rwth-aachen.de>, Jonas Otten
// @copyright 2019, Institute for Automation of Complex Power Systems, EONERC
// @license GNU General Public License (version 3)
//
// DistAIX Web
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

let Requests = {
    get(url, callback){
        fetch(url).then((res)=>{
            //console.log(res);
            if(!res.ok){
                callback(undefined, {status: res.status, message: res.statusText})
            }else{
                return res.json();
            }
        }).then((json)=>{
            callback(json, undefined);
        }).catch((err)=>callback(undefined, {status: "", message: err}));
    },
    delete(url, callback){
        let init = {method:'DELETE'};
        fetch(url, init).then((res)=>{
            if(!res.ok){
                console.log("error");
                return callback(undefined, {status: res.status, message: res.statusText})
            }
            callback(res.statusText, undefined);
        }).catch((err)=>callback(undefined, {status: "", message: err}));
    },
    post(url, body, callback){
        let init = {method:'POST',body: JSON.stringify(body)};
        fetch(url, init).then((res)=>{
            if(!res.ok){
                console.log("error");
                return callback(undefined, {status: res.status, message: res.statusText})
            }
            callback(res.statusText, undefined);
        }).catch((err)=>callback(undefined, {status: "", message: err}))
    }
}

export default Requests;