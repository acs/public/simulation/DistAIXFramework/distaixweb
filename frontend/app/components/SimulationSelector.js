// SimulationSelector class
//
// @author Sonja Happ <sonja.happ@eonerc.rwth-aachen.de>, Jonas Otten
// @copyright 2019, Institute for Automation of Complex Power Systems, EONERC
// @license GNU General Public License (version 3)
//
// DistAIX Web
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

import React from "react";
import Requests from "./requests/requests";

class SimulationSelector extends React.Component{
    constructor(props){
        super(props);
        this.state = {simulations: [], selectedSimulation: -1};
    }
    componentDidMount(){
        Requests.get('/api/v1/simulations', (res, err)=>{
            //this.props.setLoading(false);
            if(err){
                this.props.onError(err);
                return;
            } else if (!Array.isArray(res)) {
                this.props.onError({status:"", message: "simulations is not an array"})
            }
            let simulationIDs = res.map((el)=>{
                return el.simulationid;
            });
            this.setState({simulations: simulationIDs});
        });
    }
    selectedSimulation(id){
        if(id === this.state.selectedSimulation)
            id = -1; //reset selected simulation
        this.props.onChange(id);
        this.setState({selectedSimulation: id});
    }
    render(){
        let simDivs = this.state.simulations.map((el)=>{
            let style ={}
            if(el === this.state.selectedSimulation)
                style = {backgroundColor: 'crimson'};
            return(
                <div style={style} className="columnrow" onClick={this.selectedSimulation.bind(this, el)}>{el}</div>
            )
        });
        return(
            <div className="selectorColumn">
                <div className="selectorColumnHeader">Select a simulation ID</div>
                <div className="selectorColumnElements">{simDivs}</div>
            </div>
        )
    }
}

export default SimulationSelector;