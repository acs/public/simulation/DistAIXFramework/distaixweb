// DeleteGraph class
//
// @author Sonja Happ <sonja.happ@eonerc.rwth-aachen.de>, Jonas Otten
// @copyright 2019, Institute for Automation of Complex Power Systems, EONERC
// @license GNU General Public License (version 3)
//
// DistAIX Web
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

import React from 'react';
import Requests from './requests/requests.js';

import '../css/components/DeleteGraph.scss';

//Component to delete specific Graphs from the Sidebar and the backend
class DeleteGraph extends React.Component{
    constructor(props){
        super(props);
        this.state = {selectedIds: []};
    }
    onclick(el,i){
        // either delete it if it's already selected or add it
        let selectedIds = this.state.selectedIds;
        let index = selectedIds.indexOf(el);
        if(index === -1){
            selectedIds.push(el);
        }else{
            selectedIds.splice(index, 1);
        }
        this.setState({selectedIds: selectedIds});
    }
    onDelete(){
        //iterate through all selected sidebar IDs
        //for each ID send a DELETE request
        let r = this;
        this.state.selectedIds.forEach((targetId)=>{
            Requests.delete("/api/v1/sidebar/"+targetId, (res, err)=>{
                //r.props.setLoading(false);
			    if(err){
				    r.props.onError("Could not delete Graph ["+err.status+"] "+err.message);
				    return;
			    }
                // filter the deleted ID out of the current sidebar array
                let newSidebar = r.props.graphs.filter( el => el.id !== targetId);
                //propagate it to the parent object, so it can update the sidebar
                r.props.onDelete(newSidebar);
                
                //and remove it from the selectedIDs array in the current object
                let selectedIds = this.state.selectedIds.filter(id => id !== targetId)
                r.setState({selectedIds: selectedIds});
            });
        });
        
    }
    render(){
        let graphEntries = [];
        if (this.props.graphs !== undefined) {
            graphEntries = this.props.graphs.map((el, i) => {
                let style = {}
                if (this.state.selectedIds.includes(el.id)) {
                    style.backgroundColor = "crimson"
                }
                return <div key={i} style={style} className="GraphDeleteItem"
                            onClick={this.onclick.bind(this, el.id, i)}>
                    <div className="GraphDeleteItemInner">{el.name}</div>
                </div>
            })
        }
        let Button = <div id="GraphDeleteButton" onClick={this.onDelete.bind(this)} style={{cursor: "pointer"}}>Delete</div>;
        if(this.state.selectedIds.length === 0){
            Button = <div id="GraphDeleteButton" style={{opacity: 0.4, cursor:"default"}}>Delete</div>;
        }
		return(
            <div id="GraphDelete">
                <div id="GraphDeleteHeader">Please select an item to delete</div>
                <div id="graphDeleteItems">
                    {graphEntries}
                </div>
                <div id="GraphDeleteConfirm">{Button}</div>
            </div>
        )
    }
}

export default DeleteGraph;

