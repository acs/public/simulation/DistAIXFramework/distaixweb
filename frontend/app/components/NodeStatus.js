// NodeStatus class
//
// @author Sonja Happ <sonja.happ@eonerc.rwth-aachen.de>, Jonas Otten
// @copyright 2019, Institute for Automation of Complex Power Systems, EONERC
// @license GNU General Public License (version 3)
//
// DistAIX Web
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

import React from 'react';
import Requests from './requests/requests.js';
import "../css/components/NodeStatus.scss";

const oldAfterSeconds = 60*2; //2 minutes

class NodeStatus extends React.Component{
    constructor(props){
        super(props);
        this.refreshId=0;
        this.state = {status: []};
    }
    componentDidMount(){
        this.load();
        this.refreshId = setInterval(this.load.bind(this), 60 * 1000); //update every minute 
    }
    componentWillUnmount(){
        clearInterval(this.refreshId);
    }
    load(){
        Requests.get("/api/v1/nodestatus",(res, err)=>{
			if(err){
				this.props.onError(err.message);
                this.setState({status: []})
				return;
			} else if (!Array.isArray(res)) {
                this.props.onError("nodestatus is not an array");
                return;
            }
            this.setState({status: res})
        });
    }
    render(){
        let elements = []
        if(this.state.status !== undefined){
            elements = this.state.status.map((el)=>{
                let lastUpdated = new Date(el.updated);
                let diffSeconds = (Date.now().valueOf() - lastUpdated.valueOf()) / 1000;
                let style={}
                if(diffSeconds>=oldAfterSeconds){
                    style={opacity: 0.4};
                }
                return(
                    <div className="nodeStatusElement" style={style}>
                        <div className="nodeStatusElementName">{el.name}</div>
                        <div className="nodeStatusElementStatus">{el.status}</div>
                    </div>
                );
            });
        }
        return(
            <div id="nodeStatus">
                <div id="nodeStatusHeader">Node Status</div>
                <div id="nodeStatusElements">{elements}</div>
            </div>
            
        )
    }
}

export default NodeStatus