// PreparedResults class
//
// @author Sonja Happ <sonja.happ@eonerc.rwth-aachen.de>, Jonas Otten
// @copyright 2019, Institute for Automation of Complex Power Systems, EONERC
// @license GNU General Public License (version 3)
//
// DistAIX Web
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

import React from 'react';
import '../css/components/PreparedResults.scss';
import SimulationSelector from "./SimulationSelector";
import ResultTypeSelector from "./ResultTypeSelector";
import AggregateSelector from "./AggregateSelector";
import DownloadCSV from "./DownloadCSV";

const URLTEMPLATE = "/api/v1/simulations/%SIMULATIONID%/customresults/%AGGREGATETYPE%?resulttypes=%RESULTTYPES%&start=%START%&stop=%STOP%";

const aggregateTypes = {
    'Timestep':'per-timestep-aggregation.csv',
    'Agents':'per-agent-aggregation.csv'
};

// PreparedResults use (currently) two backend endpoints to download prepared results
// it contains 4 columns (SimulationSelector, ResultTypeSelector, AggregateSelector, DownloadCSV)
// the idea is to go from left to right, and while doing that select specific values for the components
class PreparedResults extends React.Component{
    constructor(props){
        super(props);
        this.state = {simulationID: -1, aggregateType: '', resulttypes: [], start: 0, stop: 0};
    }
    add(el){
        let els = this.state.elements;
        els.push(el);
        this.setState({elements: els});
    }
    //BEGIN CALLBACKS (for the components)
    changedSimulation(id){
        this.setState({simulationID: id});
    }
    resulttypeUpdate(resulttypes){
        this.setState({resulttypes: resulttypes});
    }
    changeAggregateSelector(el){
        this.setState({aggregateType: el});
    }
    onUpdateTimes(start, stop){
        this.setState({start: start, stop: stop});
    }
    onStartChange(el){
        this.setState({start: el.target.value});
    }
    onStopChange(el){
        this.setState({stop: el.target.value});
    }
    //END CALLBACKS
    download(){
        // generate the resulttTypeString for the backend
        // which is concatting an array of objects to one string
        // e.g. [{agenttypeid: 5, resulttype: "a"} , {agenttypeid: 6_3, resulttype: "b"}] --> "5|a,6_3|b"
        let resulltypesString = this.state.resulttypes.reduce((acc, el)=>{
            if(acc==="")
                return el.agenttypeid+"|"+el.resulttype;
            return acc+","+el.agenttypeid+"|"+el.resulttype;
        },"");
        //replace all placeholders in the template with the specific values
        window.location = URLTEMPLATE
            .replace("%SIMULATIONID%", this.state.simulationID)
            .replace("%AGGREGATETYPE%", aggregateTypes[this.state.aggregateType])
            .replace("%RESULTTYPES%", resulltypesString)
            .replace("%START%", this.state.start)
            .replace("%STOP%", this.state.stop);
    }
    onError(err){
        this.props.onError("Error ["+err.status+"]: "+err.message);
    }
    render(){
        let downloadAllowed = this.state.aggregateType !== '' && this.state.resulttypes.length > 0 && this.state.simulationID !== -1;
        return(
        <div id="preparedResultsColumns">
            <SimulationSelector
                onChange={this.changedSimulation.bind(this)}
                onError={this.onError.bind(this)} />
            <ResultTypeSelector
                simulationID={this.state.simulationID}
                update={this.resulttypeUpdate.bind(this)}
                onUpdateTimes={this.onUpdateTimes.bind(this)}
                onError={this.onError.bind(this)}/>
            <AggregateSelector
                onChange={this.changeAggregateSelector.bind(this)}
                onError={this.onError.bind(this)}
                aggregateTypes={aggregateTypes}
            />
            <DownloadCSV allowed={downloadAllowed} onClick={this.download.bind(this)} start={this.state.start} stop={this.state.stop} onStopChange={this.onStopChange.bind(this)} onStartChange={this.onStartChange.bind(this)} onError={this.onError.bind(this)}/>
        </div>
        ); 
    }
}

export default PreparedResults;
