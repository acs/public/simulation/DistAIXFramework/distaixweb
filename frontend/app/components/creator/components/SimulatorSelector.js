// SimulatorSelector class
//
// @author Sonja Happ <sonja.happ@eonerc.rwth-aachen.de>, Jonas Otten
// @copyright 2019, Institute for Automation of Complex Power Systems, EONERC
// @license GNU General Public License (version 3)
//
// DistAIX Web
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

import React from 'react';
import Requests from '../../requests/requests.js';
import '../../../css/components/creator/components/SimulationSelector.scss';

class SimulatorSelector extends React.Component{
	constructor(props){
		super(props);
		this.state = {data: []};
	}
	componentDidMount(){
		this.loadData();
	}
	loadData(){
		this.props.setLoading(true);
		Requests.get("/api/v1/simulations",(res, err)=>{
			this.props.setLoading(false);
			if(err){
				this.props.onError("Could not get simulations ["+err.status+"] "+err.message);
				return;
			} else if (!Array.isArray(res)){
				this.props.onError("Simulator Selector: simulations is not an array")
				return;
			}
			this.setState({data: res});
		});
	}
	simulationSelected(simid){
		if(this.props.simulationid === simid){
			this.props.selectedSimulation(-1);
		}else{
			this.props.selectedSimulation(simid);
		}
	}
	render(){
		let r = this;
		let tableElementsUnflattened = [];
		if ( this.state.data !== undefined) {
			tableElementsUnflattened = this.state.data.map((el, i) => {
				// is the current element selected
				let isSelected = el.simulationid === this.props.simulationid;
				// map the description elements
				let simDesc = el.metadata.map((meta) => {
					//if meta id is equal to -1 it's not useful so just ignore it
					if (meta.id === -1) {
						return <div key={i+"-"+ el.simulationid + "-" + meta.metatype + "-" + meta.key}>{meta.metatype}: {meta.key} {meta.value}</div>
					} else {
						return <div key={i+"-"+ el.simulationid + "-" +  meta.metatype + "-" + meta.key+ "-" + meta.id}>{meta.metatype}[{meta.id}]: {meta.key} {meta.value}</div>
					}
				});
				// set a custom style for selected elements, otherwise style is empty
				let style = isSelected ? {backgroundColor: "teal"} : {};
				//return the element itself and it's description (2 element array)
				return ([<div key={i} className="simulationTableRow"
							  onClick={r.simulationSelected.bind(this, el.simulationid)}>
					<div key={"" + i + "1"} className="simulationTableElement" style={style}>{el.simulationid}</div>
					<div key={"" + i + "2"} className="simulationTableElement" style={style}>{el.start}</div>
					<div key={"" + i + "3"} className="simulationTableElement" style={style}>{el.stop}</div>
				</div>
					,
					<div key={"" + i + "a"} style={{display: isSelected ? "inline" : "none"}}
						 className="simulationDetails">
						<div>{simDesc}</div>
					</div>
				]);
			});
		}
		//this maps this [[1,2],[3,4]] to this [1,2,3,4]
		let tableElements = [].concat.apply([], tableElementsUnflattened);
		let table = (
			<div id="simulationSelectTable">
				<div className="simulationTableRowHeader">
					<div className="simulationTableElement">ID</div>
					<div className="simulationTableElement">Start</div>
					<div className="simulationTableElement">Stop</div>
				</div>
				{tableElements}
			</div>
		);

		return (	
		<div id="mainWrapper">
			<div id="simulationSelectorHeadline"> Please select a simulation </div>
			{table}
		</div>
		);
	}
}

export default SimulatorSelector;
