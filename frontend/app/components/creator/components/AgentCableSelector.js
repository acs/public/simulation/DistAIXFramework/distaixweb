// AgentCableSelector class
//
// @author Sonja Happ <sonja.happ@eonerc.rwth-aachen.de>, Jonas Otten
// @copyright 2019, Institute for Automation of Complex Power Systems, EONERC
// @license GNU General Public License (version 3)
//
// DistAIX Web
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

import React from 'react';
import Requests from '../../requests/requests.js';
import '../../../css/components/creator/components/AgentCableSelector.scss';

// Select specific AgentIDs
// show infos for those agents (metadata)
// select all agents at once
class AgentCableSelector extends React.Component{
    constructor(props){
        super(props);
        // agents:              response from backend
        // agentsSelected:      e.g. [{agentTypeName : "PvResult", "agentTypeID": 1, agentID: "219"}]
        // metadata:            metadata field from agents, e.g. {"S_r": 5000, "Vnom": 400, "pf": 0, "pf_min": 0.8}
        // infoAgentTypeName    agentTypeName, set when this.showInfo gets called
        // infoAgentID          agentID, set when this.showInfo gets called
		this.state = {agents:[], agentsSelected:[], metadata:{}, infoAgentTypeName: "", infoAgentID: -1};
    }
    componentDidMount(){
        // initial load
		this.loadData();
    }
    // load the agents
    loadData(){
        let agenttypes = this.props.filters.map((el)=>{return el.id}).join(",");
        this.props.setLoading(true);
        Requests.get("/api/v1/simulations/"+this.props.simulationid+"/agents?agenttypes="+agenttypes, (res, err) =>{
            this.props.setLoading(false);
            if(err){
                this.props.onError("Could not get agents ["+err.status+"] "+err.message);
                return;
            } else if (!Array.isArray(res)){
                this.props.onError("agentCableSelector: agents is not an array")
                return;
            }
            this.setState({agents:res});
        });
    }
    // onClick method for the agent
    // checks if the agent is already in the agentsSelected state array (by using this.isSelected)
    // and if so, remove it, otherwise add it
    agentSelected(AgentTypeID, AgentTypeName, AgentID){
        let agentSelected = this.state.agentsSelected;
        let index = this.isSelected(AgentTypeID, AgentID);
        // selected
        if(index === -1){
            agentSelected.push({
                agentTypeName: AgentTypeName,
                agentTypeID: AgentTypeID,
                agentID: AgentID
            });
        // unselected
        }else{
            agentSelected.splice(index, 1);
        }
        // propagate to the parent, which agents are selected
        // and if all agents are selected
        // also update the state array corresponding to the generated array
        this.props.selectedAgentCables(agentSelected);
        this.props.allSelected(agentSelected.length === this.getAgentCount())
        this.setState({agentsSelected:agentSelected});
    }
    // this method checks if the combination of AgentTypeID and AgentID
    // already exists in the state agentsSelected array
    // and returns the index if it does. Otherwise -1
    isSelected(AgentTypeID, AgentID){
        for(let i = 0; i < this.state.agentsSelected.length; i++){
            let el = this.state.agentsSelected[i];
            if(el.agentTypeID === AgentTypeID && el.agentID === AgentID){
                return i;
            }
        }
        return -1;
    }
    // an onClick method to show the metadata foran agent
    showInfo(metadata, agentTypeName, agentID){
        this.setState({metadata: metadata, infoAgentTypeName: agentTypeName, infoAgentID: agentID});
    }
    // onClick method to select all/no agents
    // when all agents are selected this method will select no agent at all
    // otherwise all agents will get selected
    onAllNone(){
        //if all are marked, mark none and return
        if(this.state.agentsSelected.length === this.getAgentCount()){
            this.setState({agentsSelected: []});
            return;
        }
        let agentSelected = [];
        //iterate through all agenttypes, and through all agents for that agenttype
        this.state.agents.forEach((agenttype)=>{
            agenttype.agents.forEach((agent)=>{
                //add it to the selected array
                agentSelected.push({
                    agentTypeName: agenttype.agenttypename,
                    agentTypeID: agenttype.agenttypeid,
                    agentID: agent.agentid
                });
            });
        });
        //propagate it to the parent, and if all agents are selected (which should be the case)
        this.props.selectedAgentCables(agentSelected);
        this.props.allSelected(agentSelected.length === this.getAgentCount())
        this.setState({agentsSelected: agentSelected});
    }
    //agentCount method, because it's a nested array
    getAgentCount(){
        return this.state.agents.reduce ((sum, agenttype)=>{
            return sum + agenttype.agents.length;
        }, 0);
    }
    render(){
        let r = this;
        //iterate through all agenttypes and all agents for that agenttype
        let elements = this.state.agents.map((agenttype)=>{
            let agents = agenttype.agents.map((agent)=>{
                //not selected
                if(r.isSelected(agenttype.agenttypeid, agent.agentid) === -1){
                    return <div key={"agentCablesElement"} className="agentCablesElement">
                        <div key={"agentCablesInnerElement-" + agent.agentid} className="agentCablesInnerElement"
                            onClick={r.agentSelected.bind(r, agenttype.agenttypeid, agenttype.agenttypename, agent.agentid)}>
                            {agent.agentid}
                        </div>
                        <div key={"agentCablesInfo-" + agent.agentid} className="agentCablesInfo">
                            <i
                                className="fa fa-info-circle"
                                aria-hidden="true"
                                style={{padding: "10px"}}
                                onClick={r.showInfo.bind(r, agent.metadata, agenttype.agenttypename, agent.agentid)}/>
                        </div>
                    </div>
                }
                //selected
                return <div key={"agentCablesElement"} style = {{background:"teal"}} className="agentCablesElement" >
                    <div
                        className="agentCablesInnerElement"
                        key={"agentCablesInnerElement-" + agent.agentid}
                        onClick={r.agentSelected.bind(r, agenttype.agenttypeid, agenttype.agenttypename, agent.agentid)}>
                        {agent.agentid}
                    </div>
                    <div
                        className="agentCablesInfo">
                        <i  className="fa fa-info-circle"
                            key={"agentCablesInfo-" + agent.agentid}
                            aria-hidden="true"
                            style={{padding: "10px"}}
                            onClick={r.showInfo.bind(r, agent.metadata, agenttype.agenttypename, agent.agentid)}
                        />
                    </div>
                </div>
            });
            //return the agents for a specific agenttype
            return( 
            <div key={"agentCablesWrapper"} className="agentCablesWrapper">
                <div key={"agentCablesElementHeader"} className="agentCablesElementHeader">{agenttype.agenttypename} ({agents.length})</div>
                <div key={"agentCablesElements"} className="agentCablesElements">{agents}</div>
            </div>
            )
        });
        let keys = [];
        for(let k in this.state.metadata) keys.push(k);

        let infos = keys.map((el)=>{
            return (<div>{el}: {this.state.metadata[el]}</div>)
        });
        if (infos.length > 0){
            // add a new element at the beginning of the infos array (the headline)
            // unshift just does that (inserting sth at the beginning of the array)
            infos.unshift(<div style={{textDecoration: "underline"}}>Metadata {this.state.infoAgentTypeName}[{this.state.infoAgentID}]</div>) 
        }
        let selectAllNoneText="Select All";
        if(this.state.agentsSelected.length === this.getAgentCount()){
            selectAllNoneText="Select None";
        }
        return (<div id="agentCableSelector">
            <div id="agentCablesHeader">
                <div id="agentCableSelectText">Please select Agents/Cables IDs</div>
                <div id="agentCableSelectAllNoneButton" onClick={this.onAllNone.bind(this)}>{selectAllNoneText}</div>
            </div>
            <div id="agentCableSelectorBox">
                <div id="agentCableSelectorBoxElements">{elements}</div>
                <div id="agentCableSelectorBoxDescription">{infos}</div>
            </div>
        </div>);
    }
}
export default AgentCableSelector;
