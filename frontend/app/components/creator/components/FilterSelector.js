// FileSelector class
//
// @author Sonja Happ <sonja.happ@eonerc.rwth-aachen.de>, Jonas Otten
// @copyright 2019, Institute for Automation of Complex Power Systems, EONERC
// @license GNU General Public License (version 3)
//
// DistAIX Web
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

import React from 'react';
import Requests from '../../requests/requests.js';
import '../../../css/components/creator/components/FilterSelector.scss';

class FilterSelector extends React.Component{
	constructor(props){
		super(props);
		this.state = {data:[],clicked:-1};
	}
	componentDidMount(){
		this.loadData();
	}
	more(i,e){
		e.stopPropagation();
		this.setState({clicked: this.state.clicked === i ? -1 : i});
	}
	clicked(el){
		// check if element already in array, to determine if selected or unselected
		let index = containsElement(this.props.filters, el);
		let newFilters = this.props.filters;
		if(index !== -1){
			newFilters.splice(index, 1);
		}else{
			newFilters.push(el);
		}
		this.props.selectedFilters(newFilters);
	}
	loadData(){
		this.props.setLoading(true);
		Requests.get("/api/v1/simulations/"+this.props.simulationid+"/agenttypes",(res, err) => {
			this.props.setLoading(false);
			if(err){
				this.props.onError("Could not get agenttypes ["+err.status+"] "+err.message);
				return;
			} else if (!Array.isArray(res)){
				this.props.onError("Filter Selector: agents is not an array")
				return;
			}
			let agents = res.map((element)=>{
				return {
					id: element.id,
					name: element.type,
					description: element.resulttypes
				}
			});
			this.setState({data: agents});
		});
	}
	render(){
		let agentElements = this.state.data.map((el,i)=>{
			let selected = containsElement(this.props.filters, el) !== -1;
			let style = {};
			if(selected){
				style={
					backgroundColor: "teal"
				}
			}
			return (
				<div key={i} className="agentSelectorElement" style={style} onClick={this.clicked.bind(this, el)}>
					<div className="agentSelectorElementMain">{el.id} {el.name}</div>
					<i
						className="fa fa-info-circle"
						aria-hidden="true"
						onClick={this.more.bind(this, i)}
						style={{fontSize: "20px",	padding: "10px"}}
					/>
				</div>);
		});
		let desc;
		if(this.state.clicked === -1){
			desc=(<div/>);
		}else{
			let descEls = this.state.data[this.state.clicked].description.map((el,i)=>{
				return <div>{el}</div>
			});
			desc=(<div>
				<div>Available Resulttypes:</div>
				<div>{descEls}</div>
			</div>);
		}
		return(<div id="mainwrapper">
				<div>Please select one or more types</div>
				<div id="filterContainer">
					<div id="agentSelector">{agentElements}</div>
					<div id="agentSelectorDescription">{desc}</div>
				</div>
				</div>);
	}
}

// checks if arr contains an object with the name and id from el. returns the index or -1 if not found.
function containsElement(arr, el){
	for(let i=0;i<arr.length;i++){
		let agent = arr[i];
		if(agent.id === el.id && agent.name === el.name){
			return i;
		}
	}
	return -1;
}

export default FilterSelector;
