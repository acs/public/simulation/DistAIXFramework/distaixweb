// GraphPreview class
//
// @author Sonja Happ <sonja.happ@eonerc.rwth-aachen.de>, Jonas Otten
// @copyright 2019, Institute for Automation of Complex Power Systems, EONERC
// @license GNU General Public License (version 3)
//
// DistAIX Web
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

import React from 'react';
import DistAIXGraph from '../../DistAIXGraph.js';
import loadData from '../../requests/loadData.js';
import Requests from '../../requests/requests.js';
import '../../../css/components/creator/components/GraphPreview.scss';

class GraphPreview extends React.Component{
	constructor(props){
		super(props);
		this.state = {start: 0, stop: 0, data: [], leftKeys:[], rightKeys: [], showGraph: false};
	}
	setName(e){
		this.props.setName(e.target.value);
	}
	componentDidMount(){
		//this.load();
		this.loadStartStop();
	}
	showGraph(){
		this.load();
		this.setState({showGraph: true});
	}
	downloadNow(){
		let base = "/api/v1/simulations/"+this.props.simulation+"/results/filtered/results.csv?"
		let agentStr = "";
		if(!this.props.allSelected)
			agentStr = "&agents="+this.props.agents.map((el)=>{return el.agentID}).join(",");
		let resultTypeStr = "";
		resultTypeStr += this.props.right.reduce((str, el, i)=>{
			let r = str +el.agentTypeID+"|"+el.name
			if(i !== this.props.right.length-1){//not last element
				r+=","
			}
			return r
		}, "");
		let startStr = ",";
		if(this.props.right.length === 0 && this.props.left.length > 0){
			startStr="";
		}
		resultTypeStr += this.props.left.reduce((str, el, i)=>{
			let r = str +el.agentTypeID+"|"+el.name
			if(i !== this.props.left.length-1){//not last element
				r+=","
			}
			return r
		}, startStr);
		resultTypeStr = "&resulttypes="+resultTypeStr;
		base += "start="+this.state.start+"&stop="+this.state.stop;
		base += resultTypeStr;
		base += agentStr;
		//create filter string

		window.location = base;
	}
	load(){
		this.props.setLoading(true);
		loadData.withTypes(this.props.simulation, -1, -1, this.props.agents, this.props.right, this.props.left, (elements, err)=>{
			this.props.setLoading(false);
			if(err){
				this.props.onError("Could not load data for preview ["+err.status+"] "+err.message);
				return;
			}
			this.setState({data: elements.data, leftKeys: elements.leftKeys, rightKeys: elements.rightKeys});
		});
	}
	loadStartStop(){
		Requests.get("/api/v1/simulations/"+this.props.simulation+"/times",(res, err)=>{
			this.props.setLoading(false);
			if(err){
				this.props.onError(err);
				return err;
			}
			this.setState({start: res.starttime, stop: res.stoptime});
		});
	}
	onStartChange(el){
		this.setState({start: el.target.value})
	}
	onStopChange(el){
		this.setState({stop: el.target.value})
	}
	render(){
		let simid = this.props.simulation;
		let filterDivs = this.props.agents.map((el,i)=>{
			return <div key={i} className="previewFilterElement">{el.agentTypeName} {el.agentID}</div>
		});
		let leftAxis = this.props.left.map((el,i)=>{
			return (<div key={i} className="previewAxisLine">{el.id} {el.name} {el.agent}</div>);
		});
		let rightAxis = this.props.right.map((el,i)=>{
			return (<div key={i} className="previewAxisLine">{el.id} {el.name} {el.agent}</div>);
		});
		let graph;
		if(this.state.showGraph){
			graph = <DistAIXGraph  data={this.state.data} simulation={this.props.simulation} agents={this.props.agents} leftKeys={this.state.leftKeys} rightKeys={this.state.rightKeys} />
		}else{
			graph = <div>
					<div className="GraphPreviewButton" onClick={this.showGraph.bind(this)}>Show Graph</div>
					<div className="GraphPreviewButton" onClick={this.downloadNow.bind(this)}>Download CSV</div> 
					<div>Start <input style={{padding: "5px"}} type="number" value={this.state.start} onChange={this.onStartChange.bind(this)}/></div>
					<div>Stop <input  style={{padding: "5px"}} type="number" value={this.state.stop}  onChange={this.onStopChange.bind(this)}/></div>
				</div>
		}
		return(<div id="mainwrapper" style={{display: "flex", flexDirection: "row" }}>
				<div id="previewColumnLeft">
					<div>
						<div>Name:</div>
						<input type="text" name="graphName" onChange={this.setName.bind(this)}/>
					</div>
					<div>
						<div>Simulation: {simid}</div>
					</div>
					<div >
						<div>Selected Agents/Cables IDs:</div>
						<div id="previewSelectedAgents">{filterDivs}</div>
					</div>
					<div>
						<div>Left Axis:</div>
						<div>{leftAxis}</div>
					</div>
					<div>
						<div>Right Axis:</div>
						<div>{rightAxis}</div>
					</div>
				</div>
				{graph}
			</div>);
	}
}

export default GraphPreview;

