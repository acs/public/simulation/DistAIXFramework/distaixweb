// ResultTypeSelector class
//
// @author Sonja Happ <sonja.happ@eonerc.rwth-aachen.de>, Jonas Otten
// @copyright 2019, Institute for Automation of Complex Power Systems, EONERC
// @license GNU General Public License (version 3)
//
// DistAIX Web
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

import React from 'react';
import Requests from '../../requests/requests.js';
import '../../../css/components/creator/components/ResultTypeSelector.scss';

class ResultTypeSelector extends React.Component{
	constructor(props){
		super(props);
		this.state = {dataLeft:[],dataRight:[],dataSelectable:[]};
	}
	loadData(){
		this.props.setLoading(true);
		Requests.get("/api/v1/simulations/"+this.props.simulationid+"/agenttypes",((res, err)=>{
			this.props.setLoading(false);
			if(err){
				this.props.onError("could not get agenttypes ["+err.status+"] "+err.message);
				return;
			} else if (!Array.isArray(res)){
				this.props.onError("ResultsTypeSelector: agenttypes is not an array")
				return;
			}
			let filters = this.props.filters;
			//
			res = res.filter((el)=>{
				//just return corresponding elements
				for(let i=0;i<filters.length;i++){
					if(filters[i].name === el.type)
						return true;
				}
				return false;
			});
			res = res.map((ela)=>{
				return ela.resulttypes.map((elr)=>{
					return {name: elr, agenttype:ela.type, agentTypeID: ela.id};
				});
			});
			//flatten the array
			res = [].concat.apply([], res);
			this.setState({dataSelectable: res});
		}).bind(this));
	}
	componentDidMount(){
		this.setState({dataLeft:[],dataRight:[],dataSelectable:[]});
		this.loadData();
	}
	toLeft(el){
		let centerData  = this.removeElement(this.state.dataSelectable, el);
		let leftData   	= this.addElement(this.state.dataLeft, el);
		// set new data
		this.setState({dataSelectable: centerData, dataLeft: leftData});
		this.updateResultsParent();
	}
	toRight(el){
		let centerData  = this.removeElement(this.state.dataSelectable, el);
		let rightData   = this.addElement(this.state.dataRight, el);
		
		// set new data
		this.setState({dataSelectable: centerData, dataRight: rightData});
		this.updateResultsParent();
	}
	fromRightToCenter(el){
		let centerData  = this.addElement(this.state.dataSelectable, el);
		let rightData   = this.removeElement(this.state.dataRight, el);
		
		// set new data
		this.setState({dataSelectable: centerData, dataRight: rightData});
		this.updateResultsParent();
	}
	fromLeftToCenter(el){
		let centerData = this.addElement(this.state.dataSelectable, el);
		let leftData   = this.removeElement(this.state.dataLeft, el);
		
		// set new data
		this.setState({dataSelectable: centerData, dataLeft: leftData});
		this.updateResultsParent();
	}
	// propagate selected resulttypes to parent
	updateResultsParent(){
		this.props.selectedResults(this.state.dataLeft, this.state.dataRight);
	}
	//add element to list and return list
	addElement(list, element){
		list.push(element);
		return list;
	}
	// remove element from list if available and return list
	removeElement(list, element){
		let i = list.indexOf(element);
		if (i !== -1)
			list.splice(i, 1)
		return list
	}
	createDivs(data, toLeft, toRight, react){
		let ret = {}
		// iterate through kv pairs (agenttype, name)
		for(let i=0; i < data.length;i++){
			let el = data[i];
			// if theres no key in ret, add one with the header element
			if (!(el.agenttype in ret)){
				ret[el.agenttype]=[
					(<div key={"resultTypeElementHeader-"+el.agenttype} className="resultTypeElementHeader">{el.agenttype}</div>)
				]
			}
			//add resulttype div to the map
			ret[el.agenttype].push( <div key={"resultTypeColumnElement-" + i + el.agenttype} className="resultTypeColumnElement">
					<div  key={"resultTypeElementMove-left-"+ el.agenttype} className="resultTypeElementMove" onClick={toLeft != null ? toLeft.bind(this, el) : null}>{toLeft != null ? "◀" : "" }</div>
					<div  key={"resultTypeElementName-"+ el.agenttype} className="resultTypeElementName">{el.name}</div>
					<div  key={"resultTypeElementMove-right-"+ el.agenttype} className="resultTypeElementMove" onClick={toRight != null ? toRight.bind(this, el) : null}>{toRight != null ? "▶": ""}</div>
			</div>);
		}
		// convert map to array
		let retArr = [];
		for(let k in ret){
			for(let i=0; i < ret[k].length;i++){
				retArr.push(ret[k][i]);
			}
		}

		return retArr;
	}
	render(){
		let dataL = this.createDivs(this.state.dataLeft, null, this.fromLeftToCenter, this);
		let dataS = this.createDivs(this.state.dataSelectable, this.toLeft, this.toRight, this);
		let dataR = this.createDivs(this.state.dataRight, this.fromRightToCenter, null, this)

		return(<div id="mainwrapper" style={{flexDirection: "row",display: "flex"}}>
				<div key={"resultTypeColumn-dataL"} className="resultTypeColumn">{dataL}</div>
				<div key={"resultTypeColumn-dataS"} className="resultTypeColumn">{dataS}</div>
				<div key={"resultTypeColumn-dataR"} className="resultTypeColumn">{dataR}</div>
			</div>);
	}
}

export default ResultTypeSelector;
