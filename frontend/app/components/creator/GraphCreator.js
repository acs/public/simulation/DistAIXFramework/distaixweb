// GraphCreator class
//
// @author Sonja Happ <sonja.happ@eonerc.rwth-aachen.de>, Jonas Otten
// @copyright 2019, Institute for Automation of Complex Power Systems, EONERC
// @license GNU General Public License (version 3)
//
// DistAIX Web
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

import React from 'react';
import SimulatorSelector from './components/SimulatorSelector.js';
import FilterSelector from './components/FilterSelector.js';
import AgentCableSelector from './components/AgentCableSelector.js';
import ResultTypeSelector from './components/ResultTypeSelector.js';
import GraphPreview from './components/GraphPreview.js';
import createURLQuery from '../requests/createURLQuery.js';

import '../../css/components/creator/GraphCreator.scss';


const tabs = ["Simulation","Types","Agents/Cables","Result Types","Preview"];

class GraphCreator extends React.Component{
	constructor(props){
		super(props);
		this.state={allSelected: false, simulation:-1,filter:[], resultsLeft:[], resultsRight: [], agentCables: [], activeTabs:[true,false,false,false,false],tabSelected:0,name:""};
	}
	onTabClick(i){
		this.setState({tabSelected: i});
	}
	selectedSimulation(i){
		this.updateTabs(1, i !== -1)
		this.setState({simulation:i, filter:[], results:[], agentCableIDs: []});
	}
	selectedFilters(filters){
		this.setState({filter:filters});
		this.updateTabs(2, filters.length>0)
	}
	selectedAgentCables(agentids){
		this.setState({agentCables:agentids});
		this.updateTabs(3, agentids.length>0)
	}
	selectedResults(left, right){
		this.setState({resultsRight:right, resultsLeft: left});
		let nextActive = left.length > 0 || right.length > 0;
		this.updateTabs(4, nextActive);
	}
	// handler for next button
	onNext(){
		this.setLoading(false);
		this.setState({tabSelected: this.state.tabSelected+1});
	}
	// handler for prev button
	onPrev(){
		this.setLoading(false);		
		this.setState({tabSelected: this.state.tabSelected-1});
	}
	// handler for create button
	setName(name){
		this.setLoading(false);		
		this.setState({name: name});
	}
	onCreate(){
		let resulttypes_left = createURLQuery.resulttypes(this.state.resultsLeft);
		let resulttypes_right = createURLQuery.resulttypes(this.state.resultsRight);
		let agentids = createURLQuery.agentIDsQuery(this.state.agentCables);
		this.props.onGraphCreated({
			name:this.state.name,
			simid:this.state.simulation,
			resulttypes_left:resulttypes_left,
			resulttypes_right: resulttypes_right,
			agentids:agentids
		});
	}
	updateTabs(index, activateNext){
		let activeTabs = this.state.activeTabs;
		for(let k=index;k<tabs.length;k++){
			activeTabs[k]  = false;
		}
		if(activateNext && index < tabs.length){
			activeTabs[index] = true;
		}
		this.setState({activeTabs: activeTabs});
	}
	setLoading(status){
		document.getElementById('setupLoadingOverlayWrapper').style.display = status ? "flex" : "none";
	}
	allSelected(isAllSelected){
		this.setState({allSelected: isAllSelected});
	}
	render(){
		let t = this.state.activeTabs;
		let selectedTab = this.state.tabSelected;
		let onTabClick = this.onTabClick;
		// create divs and styles for tabs
		let Tabs = tabs.map((el,i)=>{
			// style based on selected tab and activated tab
			let style = {
				color: t[i] ? "white":"gainsboro",
				cursor: t[i] ? "pointer":"default",
				textDecoration: selectedTab===i ? "underline":"none"
			};
			// current tab is active and not selected (onclicklistener);
			return t[i] && i !== selectedTab? (<div key={i} className="CreateTab" style={style} onClick={this.onTabClick.bind(this,i)}>{el}</div>)
				    : (<div id={i} key={i} className="CreateTab" style={style}>{el}</div>)
		});

		// load mainDiv accoding to selected tab
		let mainDiv;
		switch(this.state.tabSelected){
			case 0:
				mainDiv = (<SimulatorSelector
					onError={this.props.onError}
					setLoading={this.setLoading}
					selectedSimulation={this.selectedSimulation.bind(this)}
					simulationid={this.state.simulation}
				/>);
				break;
			case 1:
				mainDiv = (<FilterSelector
					onError={this.props.onError}
					setLoading={this.setLoading}
					selectedFilters={this.selectedFilters.bind(this)}
					filters={this.state.filter}
					simulationid={this.state.simulation}
				/>);
				break;
			case 2:
				mainDiv = (<AgentCableSelector
					onError={this.props.onError}
					allSelected={this.allSelected.bind(this)}
					setLoading={this.setLoading}
					selectedAgentCables={this.selectedAgentCables.bind(this)}
					filters={this.state.filter}
					simulationid={this.state.simulation}
				/>);
				break;
			case 3:
				mainDiv = (<ResultTypeSelector
					onError={this.props.onError}
					setLoading={this.setLoading}
					selectedResults={this.selectedResults.bind(this)}
					filters={this.state.filter}
					simulationid={this.state.simulation}
				/>);
				break;
			case 4:
				mainDiv = (<GraphPreview
					onError={this.props.onError}
					allSelected = {this.state.allSelected}
					setLoading={this.setLoading}
					simulation={this.state.simulation}
					agents={this.state.agentCables}
					left={this.state.resultsLeft}
					right={this.state.resultsRight}
					setName={this.setName.bind(this)}
				/>);
				break;
			default:
				mainDiv = (<div className="error">Error</div>);

		}

		// is the "Next" Button clickable?
		let nextClickable = this.state.tabSelected+1 <= this.state.activeTabs.length-1 && this.state.activeTabs[this.state.tabSelected+1];

		let nextButton, prevButton;

		// set prevButton visible/invisible, according to tab index
		if(this.state.tabSelected>0){
			prevButton=(<div className="graphCreateButtons" onClick={this.onPrev.bind(this)}>Prev</div>);
		}else{
			prevButton=(<div className="graphCreateButtonsUnshown">Prev</div>);
		}

		// set nextButton according to state; if doesn't equal to length -1it will be shown
		if(this.state.tabSelected !== tabs.length-1){
			// active
			if(nextClickable){
				nextButton=(<div className="graphCreateButtons" onClick={this.onNext.bind(this)}>Next</div>);
			//inactive
			}else{
				nextButton=(<div className="graphCreateButtons" style={{opacity: 0.4,cursor: "default" }}>Next</div>);
			}
		// nextButton equals 3? --> Finish button; which is always active (doesn't need checks by the div)
		}else{
			if(this.state.name.length===0){
				nextButton=(<div className="graphCreateButtons" style={{opacity: 0.4, cursor:"default"}}>Create</div>);
			}else{
				nextButton=(<div className="graphCreateButtons" onClick={this.onCreate.bind(this)}>Create</div>);
			}
		}
		return(
			<div id="rightCol">
			<div id="createTabs">{Tabs}</div>
			<div id="createWrapper">
			<div id="setupLoadingOverlayWrapper">
				<div id="setupLoadingOverlay">
						<i className="fa fa-circle-o-notch fa-spin fa-3x fa-fw"/>
						<span className="sr-only">Loading...</span>
				</div>
			</div>
			{mainDiv}
			</div>
			<div id="graphCreateFooter">
				{prevButton}
				{nextButton}
			</div>
			</div>
		)
	}
}

export default GraphCreator;
