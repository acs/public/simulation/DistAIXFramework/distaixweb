// AggregateSelector class
//
// @author Sonja Happ <sonja.happ@eonerc.rwth-aachen.de>, Jonas Otten
// @copyright 2019, Institute for Automation of Complex Power Systems, EONERC
// @license GNU General Public License (version 3)
//
// DistAIX Web
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

import React from "react";

class AggregateSelector extends React.Component{
    constructor(props){
        super(props);
        this.state = {selected: ''};
    }
    onChange(el){
        this.setState({selected: el});
        this.props.onChange(el);
    }
    render(){
        console.log(this.state.selected);
        let aggregateTypesDivs = Object.keys(this.props.aggregateTypes).map((el)=>{
            let style={}
            if(this.state.selected === el)
                style={backgroundColor: 'crimson'};
            return <div key={el} style={style} className="columnrow" onClick={this.onChange.bind(this,el)}>{el}</div>
        });
        return(
            <div className="selectorColumn">
                <div className="selectorColumnHeader">Aggregate per</div>
                <div className="selectorColumnElements">{aggregateTypesDivs}</div>
            </div>
        );
    }
}

export default AggregateSelector