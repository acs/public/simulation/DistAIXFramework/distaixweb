// DistAIXGraph class
//
// @author Sonja Happ <sonja.happ@eonerc.rwth-aachen.de>, Jonas Otten
// @copyright 2019, Institute for Automation of Complex Power Systems, EONERC
// @license GNU General Public License (version 3)
//
// DistAIX Web
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

import React from 'react';
import {LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend} from 'recharts';

import '../css/components/DistAIXGraph.scss';


const lineType='linear';

const colorCodes = [
	"#e6194b", 
	"#ffe119", 
	"#0082c8", 
	"#f58231", 
	"#911eb4", 
	"#46f0f0", 
	"#f032e6", 
	"#d2f53c", 
	"#fabebe", 
	"#008080", 
	"#e6beff", 
	"#aa6e28", 
	"#fffac8", 
	"#800000", 
	"#aaffc3", 
	"#808000", 
	"#ffd8b1", 
	"#000080", 
	"#808080", 
	"#000000",
	"#3cb44b"	
]


let needsUpdate = false
class DistAIXGraph extends React.Component{
	constructor(props){
		super(props);
		this.state = {width: 0, height: 0, legendWidth: 0};//, showGraph: false};
	}
	componentDidMount(){
		window.addEventListener("resize", this.updateDimensions.bind(this));
		const idName = "Graph";
		const height = document.getElementById(idName).clientHeight;
		const width  = document.getElementById(idName).clientWidth;

		let legendWidth = document.getElementById("leftLegend").clientWidth;
		legendWidth  += document.getElementById("rightLegend").clientWidth;
		//just set state if it changes
		this.setState({height: height, width: width, legendWidth: legendWidth});
		
	}
	// componentWillReceiveProps(nextProps){
	// 	if(nextProps.infoActive != this.props.infoActive){
	// 		this.setState(this.state);
	// 	}
	// }
	componentDidUpdate(prevProps, prevState){
		this.updateDimensions();
	}
	updateDimensions(){
		const idName = "Graph";
		const height = document.getElementById(idName).clientHeight;
		const width  = document.getElementById(idName).clientWidth;

		let legendWidth = document.getElementById("leftLegend").clientWidth;
		legendWidth  += document.getElementById("rightLegend").clientWidth;
		//just set state if it changes
		if(this.state.height !== height || width !== this.state.width || legendWidth !== this.state.legendWidth){
			this.setState({height: height, width: width, legendWidth: legendWidth});
		}
			
	}
	render(){
		//get keys
		let keys = []
		for(let key in this.props.data[0]){
			if(key !== "simTime"){
				keys.push(key);
			}
		}
		//var keys = this.props.data[0].filter(key => key !== "simTime")

		let leftLegendColors = [];
		let rightLegendColors = [];
		let lines = keys.map((el,i)=>{
			let color = colorCodes[i%colorCodes.length];
			if(this.props.leftKeys.indexOf(el)!== -1){
				leftLegendColors.push(color);
				return(
					<Line dataKey={el} stroke={color} type={lineType} yAxisId="left" legendType="line"/>
				)
			}
			if(this.props.rightKeys.indexOf(el)!== -1){
				rightLegendColors.push(color);
				return(
					<Line dataKey={el} stroke={color} type={lineType} yAxisId="right" legendType="cross" />
				)
			}


		})

		let leftLegend = this.props.leftKeys.map((el,i)=>{
			return <div key={"leftLegendColors-"+ el + i} style={{color: leftLegendColors[i]}}>{el}</div>;
		});
		let rightLegend = this.props.rightKeys.map((el,i)=>{
			return <div key={"rightLegendColors-"+ el + i} style={{color: rightLegendColors[i]}}>{el}</div>;
		});
		if(this.state.legendWidth === 0 ){
			needsUpdate = true;
			return(
				<div id="GraphWrapper">
					<div id="leftLegend">
						<div id="leftLegendElements">{leftLegend}</div>
					</div>
					<div id="Graph">
						<div id="placeholder" style={{flexGrow: 1}} />
					</div>
					<div id="rightLegend">
						<div id="legendRightElements">{rightLegend}</div>
					</div>
				</div>
			)
		}
		return(
			<div id="GraphWrapper">
				<div id="leftLegend">
					<div style={{textDecoration: "underline"}}>Left Axis:</div>
					<div id="leftLegendElements">{leftLegend}</div>
				</div>				
				<div id="Graph">
				<LineChart width={this.state.width} height={this.state.height} data={this.props.data} margin={{top: 5, right: 30, left: 20, bottom: 5}}>
					<XAxis dataKey="simTime"/>
					<YAxis padding={{ right: 30 }} yAxisId="left" orientation="left" domain={['auto', 'auto']}   />
					<YAxis padding={{ left: 30 }}  yAxisId="right" orientation="right" domain={['auto', 'auto']} />}/>
					<CartesianGrid strokeDasharray="3 3"/>
					<Tooltip/>
					{lines}
				</LineChart>
				</div>
				<div id="rightLegend">
						<div style={{textDecoration: "underline"}}>Right Axis:</div>
						<div id="legendRightElements">{rightLegend}</div>
				</div>
			</div>
		)
	}
}

export default DistAIXGraph;
 