// DownloadCSV class
//
// @author Sonja Happ <sonja.happ@eonerc.rwth-aachen.de>, Jonas Otten
// @copyright 2019, Institute for Automation of Complex Power Systems, EONERC
// @license GNU General Public License (version 3)
//
// DistAIX Web
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

import React from "react";

class DownloadCSV extends React.Component{
    render(){
        if(this.props.allowed){
            return(
                <div className="selectorColumn">
                    <div className="selectorColumnHeader" style={{color: 'white'}}>f</div>
                    <div className="Times"> <div>Start:</div> <input type="number" name="start" className="TimesInput" value={this.props.start} onChange={this.props.onStartChange} /> </div>
                    <div className="Times"> <div>Stop: </div> <input type="number" name="stop" className="TimesInput" value={this.props.stop} onChange={this.props.onStopChange} /> </div>
                    <div id="DownloadCSVButton" onClick={this.props.onClick}>Download</div>
                </div>
            );
        }
        return(
            <div className="selectorColumn">
                <div className="selectorColumnHeader" style={{color: 'white'}}>f</div>
                <div className="Times"> <div>Start:</div> <input type="number" name="start" className="TimesInput" value={this.props.start} onChange={this.props.onStartChange} /> </div>
                <div className="Times"> <div>Stop: </div> <input type="number" name="stop" className="TimesInput" value={this.props.stop} onChange={this.props.onStopChange} /> </div>
                <div id="DownloadCSVButton" style={{opacity: 0.4, cursor:"default"}}>Download</div>
            </div>
        );
    }
}

export default DownloadCSV