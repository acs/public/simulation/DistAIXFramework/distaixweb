const path = require('path');
const webpack = require('webpack');

module.exports = {
	mode: "development",
	entry: "./app/app.js",
	output:{
		filename: 'bundle.js',
		path: path.resolve(__dirname, 'public/dist'),
		publicPath: "/dist"
	},
	module: {
		rules: [
			{ test: /\.jsx?$/, exclude: /node_modules/, loader:"babel-loader"},
			{ test: /\.scss$/, exclude: /node_modules/, loader:"style-loader"},
			{ test: /\.scss$/, exclude: /node_modules/, loader:"css-loader"},
			{ test: /\.scss$/, exclude: /node_modules/, loader:"sass-loader"}
  		]
	},
	resolve: {
		extensions: ['*', '.js']
	},
	plugins:[
		new webpack.HotModuleReplacementPlugin(),
		new webpack.NoEmitOnErrorsPlugin()
	],

	devServer:{
		static: {
			directory: path.join(__dirname, 'public')
		},
		compress:true,
		port: 8080,
		proxy: {
			'/api': {
				target: 'http://localhost:4000',
				secure: false
			}
		}
	}
};
