// web package
//
// @author Sonja Happ <sonja.happ@eonerc.rwth-aachen.de>, Jonas Otten
// @copyright 2019, Institute for Automation of Complex Power Systems, EONERC
// @license GNU General Public License (version 3)
//
// DistAIX Web
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

package web

import (
	"fmt"
	"git.rwth-aachen.de/acs/public/DistAIXFramework/distaixweb.git/storage/serialization"
	"log"
	"net/http"
	"strconv"
	"strings"
)

// SendJSON is a helper method to save a byte array as json
func SendJSON(bytes []byte, rw http.ResponseWriter) {
	rw.Header().Set("Content-Type", "application/json")
	_, err := rw.Write(bytes)
	if err != nil {
		log.Printf("Could not write bytes to response: %v", err)
	}
}

func InternalError(r http.ResponseWriter, err error) {
	log.Printf("[INTERNAL SERVER ERROR] %v", err)
	r.WriteHeader(http.StatusInternalServerError)
	fmt.Fprintf(r, "Internal Server Error")
}

func CreateIntArrayFromString(r string) ([]int, error) {
	sArr := strings.Split(r, ",")
	iArr := make([]int, len(sArr))

	for i, v := range sArr {
		v64, err := strconv.Atoi(v)
		if err != nil {
			return nil, err
		}
		iArr[i] = v64
	}
	return iArr, nil
}

func GenerateKey(agentTypeName string, resultTypeName string, agentID string) string {
	return fmt.Sprintf("%s %s (%s)", agentTypeName, resultTypeName, agentID)
}

//creates a filter array from a query string
func CreateFilter(query string) ([]serialization.ResultTypeFilter, error) {
	filters := make([]serialization.ResultTypeFilter, 0)
	for _, filterString := range strings.Split(query, ",") {
		parts := strings.SplitN(filterString, "|", 2)
		agentTypeID, err := strconv.Atoi(parts[0])
		if err != nil {
			return nil, err
		}
		filters = append(filters, serialization.ResultTypeFilter{AgentTypeID: agentTypeID, ResultTypeName: parts[1]})
	}
	return filters, nil
}
