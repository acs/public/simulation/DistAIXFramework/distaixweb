// aggregatePerAgent package
//
// @author Sonja Happ <sonja.happ@eonerc.rwth-aachen.de>, Jonas Otten
// @copyright 2019, Institute for Automation of Complex Power Systems, EONERC
// @license GNU General Public License (version 3)
//
// DistAIX Web
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

package aggregatePerAgent

import (
	"context"
	"database/sql"
	"encoding/json"
	"git.rwth-aachen.de/acs/public/DistAIXFramework/distaixweb.git/storage"
	"git.rwth-aachen.de/acs/public/DistAIXFramework/distaixweb.git/storage/serialization"
	"log"
	"math"
	"net/http"
	"strconv"
	"strings"

	"github.com/gocql/gocql"
	"github.com/gorilla/mux"
)

type aggregation struct {
	count              int
	min, max, avg, sum float64
}

func Handle(cass *gocql.Session, db *sql.DB, rw http.ResponseWriter, r *http.Request) (int, error) {
	vars := mux.Vars(r)
	simid, err := strconv.Atoi(vars["simulationid"])
	if err != nil {
		return http.StatusBadRequest, err
	}

	start, err := strconv.ParseFloat(vars["start"], 64)
	if err != nil {
		return http.StatusBadRequest, err
	}
	stop, err := strconv.ParseFloat(vars["stop"], 64)
	if err != nil {
		return http.StatusBadRequest, err
	}

	filters, err := createFilter(vars["resulttypes"])
	if err != nil {
		http.Error(rw, "Internal Server Error", http.StatusInternalServerError)
		log.Printf("Could create from query %s filters: %v", vars["resulttypes"], err)
		return http.StatusInternalServerError, err
	}

	//because of the cassandra table structure we need to know the agentids
	agenttypeids := make([]int, len(filters))
	for i, f := range filters {
		agenttypeids[i] = f.AgentTypeID
	}

	agents, err := storage.GetAgentsFiltered(db, simid, agenttypeids)
	if err != nil {
		return http.StatusInternalServerError, err
	}

	agentids := make([]string, 0)
	for _, atype := range agents {
		for _, a := range atype.Agents {
			agentids = append(agentids, a.AgentID)
		}
	}
	// context makes sure we stop our work when the user cancels the request
	ctx := r.Context()
	ch := make(chan result)
	go getResults(ctx, cass, simid, start, stop, agentids, filters, ch)

	//mapping from agentid to resulttypes to aggregation
	resultMap := make(map[string]map[string]*aggregation)
	for r := range ch {
		select {
		case <-ctx.Done():
			log.Println("Request canceled")
			return http.StatusNoContent, nil
		default:
			if r.err != nil {
				log.Println("Error:", r.err)
				return http.StatusInternalServerError, r.err
			}
			addResultToMap(&resultMap, &r)
		}

	}
	calcAverages(&resultMap)
	aggregatedResults := toArray(&resultMap)
	bytes, err := json.Marshal(aggregatedResults)
	if err != nil {
		return http.StatusInternalServerError, nil
	}
	sendJSON(bytes, rw)

	return http.StatusOK, nil
}

func createFilter(query string) ([]serialization.ResultTypeFilter, error) {
	filters := make([]serialization.ResultTypeFilter, 0)
	for _, filterString := range strings.Split(query, ",") {
		parts := strings.SplitN(filterString, "|", 2)
		agentTypeID, err := strconv.Atoi(parts[0])
		if err != nil {
			return nil, err
		}
		filters = append(filters, serialization.ResultTypeFilter{AgentTypeID: agentTypeID, ResultTypeName: parts[1]})
	}
	return filters, nil
}

type result struct {
	err          error
	resultValues map[string]interface{}
	agentid      string
	agenttypeid  int
}

func getResults(ctx context.Context, session *gocql.Session, simID int, start float64, stop float64, agentIDs []string, filterResultTypes []serialization.ResultTypeFilter, ch chan result) {
	q := `SELECT agentid, agenttypeid, simtime, result FROM results WHERE simid=? AND agentid IN ? AND simtime >= ? AND simtime <= ?`
	iter := session.Query(q, simID, agentIDs, start, stop).Iter()

	var dbAgentTypeID int
	var dbAgentID string
	var dbSimTime float64
	var dbSerialized []byte

	for iter.Scan(&dbAgentID, &dbAgentTypeID, &dbSimTime, &dbSerialized) {
		select {
		case <-ctx.Done():
			//send empty package
			ch <- result{}
			return
		default:
			//try to get agentType name
			var name string
			namePtr := serialization.GetAgentTypeName(dbAgentTypeID)
			if namePtr != nil {
				name = *namePtr
			}

			_, resultValues, err := serialization.DeserializeToMapWithResulttypes(dbAgentTypeID, dbSerialized, filterResultTypes)

			if err != nil {
				ch <- result{err: err}
				continue
			}
			// could not deserialize
			if resultValues == nil {
				log.Printf("Could not deserialize AgentTypeID %d %s", dbAgentTypeID, name)
				continue
			}
			ch <- result{resultValues: resultValues, agentid: dbAgentID, agenttypeid: dbAgentTypeID}
		}
	}
	if err := iter.Close(); err != nil {
		ch <- result{err: err}
	}
	close(ch)
}

func addResultToMap(resultMap *map[string]map[string]*aggregation, results *result) {
	if _, ok := (*resultMap)[results.agentid]; !ok {
		(*resultMap)[results.agentid] = make(map[string]*aggregation)
	}
	for resulttype, value := range results.resultValues {
		var vFloat64 float64
		switch value.(type) {
		case float64:
			vFloat64 = value.(float64)
			break
		case int32:
			vFloat64 = float64(value.(int32))
			break
		case uint64:
			vFloat64 = float64(value.(uint64))
		case uint32:
			vFloat64 = float64(value.(uint32))
			break
		default:
			log.Printf("Could not find corresponding type to result: %T\nSomething is wrong here, continuing", value)
			continue
		}

		aggregate, ok := (*resultMap)[results.agentid][resulttype]
		if !ok {
			aggregate = newAggregate()
		}
		//check min
		if vFloat64 < aggregate.min {
			aggregate.min = vFloat64
		}
		//check max
		if vFloat64 > aggregate.max {
			aggregate.max = vFloat64
		}
		aggregate.sum += vFloat64
		aggregate.count++
		(*resultMap)[results.agentid][resulttype] = aggregate
	}
}

func newAggregate() *aggregation {
	var a aggregation
	a.avg = 0
	a.count = 0
	a.max = -math.MaxFloat64
	a.min = math.MaxFloat64
	a.sum = 0
	return &a
}

func calcAverages(resultMap *map[string]map[string]*aggregation) {
	for agentid, values := range *resultMap {
		for resulttypeid, result := range values {
			avg := result.sum / float64(result.count)
			(*resultMap)[agentid][resulttypeid].avg = avg
		}
	}
}

type ArrayResulttype struct {
	Resulttype         string
	Min, Max, Avg, Sum float64
}

type ArrayAgent struct {
	AgentID     string
	ResultTypes []ArrayResulttype
}

func toArray(resultMap *map[string]map[string]*aggregation) *[]ArrayAgent {
	arr := make([]ArrayAgent, len(*resultMap))
	agentindex := 0
	for agentid, values := range *resultMap {
		var aa ArrayAgent
		aa.AgentID = agentid
		resulttypes := make([]ArrayResulttype, len((*resultMap)[agentid]))
		resultindex := 0
		for resulttypeid, result := range values {
			var ar ArrayResulttype
			ar.Resulttype = resulttypeid
			ar.Avg = result.avg
			ar.Max = result.max
			ar.Min = result.min
			ar.Sum = result.sum
			resulttypes[resultindex] = ar
			resultindex++
		}
		aa.ResultTypes = resulttypes
		arr[agentindex] = aa
		agentindex++
	}
	return &arr
}

func sendJSON(bytes []byte, rw http.ResponseWriter) {
	rw.Header().Set("Content-Type", "application/json")
	_, err := rw.Write(bytes)
	if err != nil {
		log.Printf("Could not write bytes to response: %v", err)
	}
}
