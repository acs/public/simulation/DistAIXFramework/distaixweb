// aggregatePerAgent package
//
// @author Sonja Happ <sonja.happ@eonerc.rwth-aachen.de>, Jonas Otten
// @copyright 2019, Institute for Automation of Complex Power Systems, EONERC
// @license GNU General Public License (version 3)
//
// DistAIX Web
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

package aggregatePerAgent

import (
	"database/sql"
	"encoding/csv"
	"fmt"
	"git.rwth-aachen.de/acs/public/DistAIXFramework/distaixweb.git/storage"
	"log"
	"net/http"
	"strconv"

	"github.com/gocql/gocql"
	"github.com/gorilla/mux"
)

func HandleCSV(cass *gocql.Session, db *sql.DB, rw http.ResponseWriter, r *http.Request) (int, error) {
	vars := mux.Vars(r)
	simid, err := strconv.Atoi(vars["simulationid"])
	if err != nil {
		return http.StatusBadRequest, err
	}

	start, err := strconv.ParseFloat(vars["start"], 64)
	if err != nil {
		return http.StatusBadRequest, err
	}
	stop, err := strconv.ParseFloat(vars["stop"], 64)
	if err != nil {
		return http.StatusBadRequest, err
	}

	filters, err := createFilter(vars["resulttypes"])
	if err != nil {
		http.Error(rw, "Internal Server Error", http.StatusInternalServerError)
		log.Printf("Could create from query %s filters: %v", vars["resulttypes"], err)
		return http.StatusInternalServerError, err
	}

	//because of the cassandra table structure we need to know the agentids
	agenttypeids := make([]int, len(filters))
	for i, f := range filters {
		agenttypeids[i] = f.AgentTypeID
	}

	agents, err := storage.GetAgentsFiltered(db, simid, agenttypeids)
	if err != nil {
		return http.StatusInternalServerError, err
	}

	agentids := make([]string, 0)
	for _, atype := range agents {
		for _, a := range atype.Agents {
			agentids = append(agentids, a.AgentID)
		}
	}
	ch := make(chan result)
	ctx := r.Context()
	go getResults(ctx, cass, simid, start, stop, agentids, filters, ch)

	resultMap := make(map[string]map[string]*aggregation)
	for r := range ch {
		select {
		case <-ctx.Done():
			log.Println("Request canceled")
			return http.StatusNoContent, nil
		default:
			if r.err != nil {
				log.Println("Error:", r.err)
				return http.StatusInternalServerError, r.err
			}
			addResultToMap(&resultMap, &r)
		}
	}
	calcAverages(&resultMap)
	csvArr := toCSVArray(&resultMap)
	w := csv.NewWriter(rw)
	rw.Header().Add("Content-Disposition", "attachment")
	rw.Header().Add("Content-Type", "text/csv")
	w.WriteAll(csvArr)
	w.Flush()
	if err := w.Error(); err != nil {
		log.Println("Error CSV:", err)
		return http.StatusInternalServerError, err
	}
	return http.StatusOK, nil
}

func toCSVArray(resultMap *map[string]map[string]*aggregation) [][]string {
	// get all agentids/resulttype combinations, to determine the columns
	resultTypeSet := make(map[string]int)

	i := 0
	for _, agent := range *resultMap {
		for rt := range agent {
			combination := fmt.Sprint(rt)
			if _, ok := resultTypeSet[combination]; !ok {
				resultTypeSet[combination] = i //mapping from resulttype to column index
				i++
			}
		}
	}

	headers := make([]string, 1)
	headers[0] = "agent"

	headerTemplate := []string{
		"min",
		"max",
		"avg",
		"sum",
	}

	//create headers
	for rt := range resultTypeSet {
		for _, ht := range headerTemplate {
			//create a header (e.g. 51[V_nom]_min)
			headers = append(headers, fmt.Sprintf("%s_%s", rt, ht))
		}
	}

	csvrows := make([][]string, 1)
	csvrows[0] = headers

	for aid, agent := range *resultMap {
		line := make([]string, len(headers))
		line[0] = aid
		for rt, r := range agent {
			startPos := resultTypeSet[fmt.Sprintf("%s", rt)]*4 + 1 //offset = 1 (agent id) + 4 * position, since 4 types per agent (min,max,avg,sum)
			line[startPos] = fmt.Sprint(r.min)
			line[startPos+1] = fmt.Sprint(r.max)
			line[startPos+2] = fmt.Sprint(r.avg)
			line[startPos+3] = fmt.Sprint(r.sum)
		}
		csvrows = append(csvrows, line)
	}

	return csvrows
}
