// agenttypes package
//
// @author Sonja Happ <sonja.happ@eonerc.rwth-aachen.de>, Jonas Otten
// @copyright 2019, Institute for Automation of Complex Power Systems, EONERC
// @license GNU General Public License (version 3)
//
// DistAIX Web
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

package agenttypes

import (
	"encoding/json"
	"git.rwth-aachen.de/acs/public/DistAIXFramework/distaixweb.git/storage"
	"git.rwth-aachen.de/acs/public/DistAIXFramework/distaixweb.git/storage/serialization"
	"git.rwth-aachen.de/acs/public/DistAIXFramework/distaixweb.git/web"
	"net/http"
	"sort"
	"strconv"

	"github.com/gorilla/mux"
)

func HandleAgentTypes(ctx *web.Context, rw http.ResponseWriter, r *http.Request) (int, error) {
	vars := mux.Vars(r)
	simid, err := strconv.Atoi(vars["simulationid"])
	if err != nil {
		web.InternalError(rw, err)
		return http.StatusInternalServerError, nil
	}

	typeIDs, err := storage.GetAgentTypeIDs(ctx.DBConn, simid)
	if err != nil {
		web.InternalError(rw, err)
		return http.StatusInternalServerError, nil
	}
	types := serialization.GetResultTypes(typeIDs)

	// append the fake timemeasurementAgent
	timeMeasurementType, err := storage.GetTimeMeasurementAgentType(ctx.DBConn, simid)
	if err != nil {
		web.InternalError(rw, err)
		return http.StatusInternalServerError, nil
	}

	types = append(types, *timeMeasurementType)

	//sort (actually again, serialization.GetResultTypes does it as well)
	sort.Sort(serialization.ByID(types))
	bytes, err := json.Marshal(&types)
	if err != nil {
		web.InternalError(rw, err)
		return http.StatusInternalServerError, nil
	}
	web.SendJSON(bytes, rw)
	return http.StatusOK, nil
}
