// nodestatus package
//
// @author Sonja Happ <sonja.happ@eonerc.rwth-aachen.de>, Jonas Otten
// @copyright 2019, Institute for Automation of Complex Power Systems, EONERC
// @license GNU General Public License (version 3)
//
// DistAIX Web
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

package nodestatus

import (
	"encoding/json"
	"git.rwth-aachen.de/acs/public/DistAIXFramework/distaixweb.git/web"
	"io/ioutil"
	"log"
	"net/http"
	"sort"
	"time"

	"github.com/gorilla/mux"
)

func HandleNodeStatusPost(ctx *web.Context, rw http.ResponseWriter, r *http.Request) (int, error) {
	vars := mux.Vars(r)
	name := vars["nodename"]
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return http.StatusInternalServerError, err
	}
	var status web.NodeStatusValue
	status.Status = string(body)
	status.Time = time.Now()
	ctx.Mtx.Lock()
	ctx.NodeStatus = removeOld(ctx.NodeStatus)
	ctx.NodeStatus[name] = status
	ctx.Mtx.Unlock()
	return http.StatusOK, nil
}

const REMOVEAFTERMINUTES = 60

func removeOld(statusMap map[string]web.NodeStatusValue) map[string]web.NodeStatusValue {
	now := time.Now()
	for name, status := range statusMap {
		diff := now.Sub(status.Time)
		if diff/time.Minute >= REMOVEAFTERMINUTES {
			log.Println("Delete " + name)
			delete(statusMap, name)
		}
	}
	return statusMap
}

type NodeStatus struct {
	Name    string `json:"name"`
	Status  string `json:"status"`
	Updated string `json:"updated"`
}

const rfc2822 = "Mon Jan 02 15:04:05 -0700 2006"

func HandleNodeStatusGet(ctx *web.Context, rw http.ResponseWriter, r *http.Request) (int, error) {
	ctx.Mtx.Lock()
	ret := make([]NodeStatus, 0)
	ctx.NodeStatus = removeOld(ctx.NodeStatus)
	for name, status := range ctx.NodeStatus {
		var nodestatus NodeStatus
		nodestatus.Status = status.Status
		nodestatus.Updated = status.Time.Format(rfc2822)
		nodestatus.Name = name
		ret = append(ret, nodestatus)
	}
	ctx.Mtx.Unlock()
	sort.Sort(sortAfterName(ret))
	bytes, err := json.Marshal(ret)
	if err != nil {
		return http.StatusInternalServerError, err
	}
	web.SendJSON(bytes, rw)
	return http.StatusOK, nil
}

type sortAfterName []NodeStatus

func (s sortAfterName) Len() int {
	return len(s)
}

func (s sortAfterName) Less(a, b int) bool {
	return s[a].Name < s[b].Name
}

func (s sortAfterName) Swap(a, b int) {
	s[a], s[b] = s[b], s[a]
}
