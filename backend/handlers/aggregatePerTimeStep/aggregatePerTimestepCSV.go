// aggregratePerTimestep package
//
// @author Sonja Happ <sonja.happ@eonerc.rwth-aachen.de>, Jonas Otten
// @copyright 2019, Institute for Automation of Complex Power Systems, EONERC
// @license GNU General Public License (version 3)
//
// DistAIX Web
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

package aggregatePerTimestep

import (
	"database/sql"
	"encoding/csv"
	"fmt"
	"git.rwth-aachen.de/acs/public/DistAIXFramework/distaixweb.git/storage"
	"log"
	"math"
	"net/http"
	"sort"
	"strconv"

	"github.com/gocql/gocql"
	"github.com/gorilla/mux"
)

func HandleCSV(cass *gocql.Session, db *sql.DB, rw http.ResponseWriter, r *http.Request) (int, error) {
	vars := mux.Vars(r)
	simid, err := strconv.Atoi(vars["simulationid"])
	if err != nil {
		return http.StatusBadRequest, err
	}

	start, err := strconv.ParseFloat(vars["start"], 64)
	if err != nil {
		return http.StatusBadRequest, err
	}
	stop, err := strconv.ParseFloat(vars["stop"], 64)
	if err != nil {
		return http.StatusBadRequest, err
	}

	filters, err := createFilter(vars["resulttypes"]) //create filters, which is an array containt filter objects that consist of agenttypeids and resulttypes
	if err != nil {
		http.Error(rw, "Internal Server Error", http.StatusInternalServerError)
		log.Printf("Could create from query %s filters: %v", vars["resulttypes"], err)
		return http.StatusInternalServerError, err
	}

	//because of the cassandra table structure we need to know the agentids beforehand
	agenttypeids := make([]int, len(filters))
	for i, f := range filters {
		agenttypeids[i] = f.AgentTypeID
	}

	agents, err := storage.GetAgentsFiltered(db, simid, agenttypeids) //get results from cassandra
	if err != nil {
		return http.StatusInternalServerError, err
	}

	agentids := make([]string, 0) //put all agentids into an array
	for _, atype := range agents {
		for _, a := range atype.Agents {
			agentids = append(agentids, a.AgentID)
		}
	}
	//channel and context for get results
	ch := make(chan result)
	ctx := r.Context()
	go getResults(ctx, cass, simid, start, stop, agentids, filters, ch) //start goroutine to get results

	resultMap := make(map[float64]map[string]*aggregation) //map from simtime to agentid to results
	for r := range ch {
		select {
		case <-ctx.Done():
			log.Println("Request canceled")
			return http.StatusNoContent, nil
		default:
			if r.err != nil {
				log.Println("Error:", r.err)
				return http.StatusInternalServerError, r.err
			}
			addResultToMap(&resultMap, &r)
		}
	}
	calcAverages(&resultMap) //calculate averages for the results in the map

	//calc variance
	ch = make(chan result)
	go getResults(ctx, cass, simid, start, stop, agentids, filters, ch) //again, need to get results to compare it to the avg
	if err := calcStddev(ctx, &resultMap, ch); err != nil {
		if err.Error() == "Request canceled" {
			log.Println("Request canceled")
			return http.StatusConflict, nil
		}
		log.Println("Error for variance calc:", err)
		return http.StatusInternalServerError, err
	}

	csvArr := toCSVArray(&resultMap) //convert the map to a 2 dimensional array for csv
	w := csv.NewWriter(rw)
	rw.Header().Add("Content-Disposition", "attachment")
	rw.Header().Add("Content-Type", "text/csv")
	w.WriteAll(csvArr)
	w.Flush()
	if err := w.Error(); err != nil {
		log.Println("Error CSV:", err)
		return http.StatusInternalServerError, err
	}
	return http.StatusOK, nil
}

func toCSVArray(resultMap *map[float64]map[string]*aggregation) [][]string {
	csvarr := make([][]string, 1)
	headers := []string{
		"min",
		"max",
		"avg",
		"stddev",
		"sum",
	}

	keys := make([]string, 0) //needed to get a strict order when inserting into columns
	for _, resulttype := range *resultMap {
		header := []string{
			"simtime",
		}
		for t := range resulttype {
			keys = append(keys, t)
			for _, h := range headers {
				header = append(header, fmt.Sprintf("%s %s", h, t))
			}
		}
		csvarr[0] = header
		break
	}

	for time, resulttype := range *resultMap {
		csvrow := make([]string, 1)
		csvrow[0] = fmt.Sprint(time)
		for _, k := range keys {
			// TODO at some point that was nil, which lead to a crash
			// I'm not sure how that was possible but it might happen again in the future
			r := resulttype[k]
			csvrow = append(csvrow, fmt.Sprint(r.min))
			csvrow = append(csvrow, fmt.Sprint(r.max))
			csvrow = append(csvrow, fmt.Sprint(r.avg))
			csvrow = append(csvrow, fmt.Sprint(math.Sqrt(r.stddevsum/float64(r.count))))
			csvrow = append(csvrow, fmt.Sprint(r.sum))
		}
		csvarr = append(csvarr, csvrow)
	}
	sort.Sort(bySimTime(csvarr[1:]))
	return csvarr
}

type bySimTime [][]string

func (s bySimTime) Len() int {
	return len(s)
}

func (s bySimTime) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}

func (s bySimTime) Less(i, j int) bool {
	//getsimtime
	a, err := strconv.ParseFloat(s[i][0], 64)
	if err != nil {
		log.Println("Error while parsing float for sorting: ", err)
		return true
	}
	b, err := strconv.ParseFloat(s[j][0], 64)
	if err != nil {
		log.Println("Error while parsing float for sorting: ", err)
		return false
	}
	return a < b
}
