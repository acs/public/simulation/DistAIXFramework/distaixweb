// aggregratePerTimestep package
//
// @author Sonja Happ <sonja.happ@eonerc.rwth-aachen.de>, Jonas Otten
// @copyright 2019, Institute for Automation of Complex Power Systems, EONERC
// @license GNU General Public License (version 3)
//
// DistAIX Web
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

package aggregatePerTimestep

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"git.rwth-aachen.de/acs/public/DistAIXFramework/distaixweb.git/storage"
	"git.rwth-aachen.de/acs/public/DistAIXFramework/distaixweb.git/storage/serialization"
	"log"
	"math"
	"net/http"
	"strconv"
	"strings"

	"github.com/gocql/gocql"
	"github.com/gorilla/mux"
)

type aggregation struct {
	agenttypeid                         int
	count                               int
	min, max, avg, sum, stddevsum, norm float64
}

func Handle(cass *gocql.Session, db *sql.DB, rw http.ResponseWriter, r *http.Request) (int, error) {
	vars := mux.Vars(r)
	simid, err := strconv.Atoi(vars["simulationid"])
	if err != nil {
		return http.StatusBadRequest, err
	}

	start, err := strconv.ParseFloat(vars["start"], 64)
	if err != nil {
		return http.StatusBadRequest, err
	}
	stop, err := strconv.ParseFloat(vars["stop"], 64)
	if err != nil {
		return http.StatusBadRequest, err
	}

	filters, err := createFilter(vars["resulttypes"])
	if err != nil {
		http.Error(rw, "Internal Server Error", http.StatusInternalServerError)
		log.Printf("Could create from query %s filters: %v", vars["resulttypes"], err)
		return http.StatusInternalServerError, err
	}

	//because of the cassandra table structure we need to know the agentids
	agenttypeids := make([]int, len(filters))
	for i, f := range filters {
		agenttypeids[i] = f.AgentTypeID
	}

	agents, err := storage.GetAgentsFiltered(db, simid, agenttypeids)
	if err != nil {
		return http.StatusInternalServerError, err
	}

	agentids := make([]string, 0)
	for _, atype := range agents {
		for _, a := range atype.Agents {
			agentids = append(agentids, a.AgentID)
		}
	}
	ctx := r.Context()
	ch := make(chan result)
	go getResults(ctx, cass, simid, start, stop, agentids, filters, ch)

	resultMap := make(map[float64]map[string]*aggregation)
	for r := range ch {
		select {
		case <-ctx.Done():
			log.Println("Request canceled")
			return http.StatusNoContent, nil
		default:
			if r.err != nil {
				log.Println("Error:", r.err)
				return http.StatusInternalServerError, r.err
			}
			addResultToMap(&resultMap, &r)
		}

	}
	calcAverages(&resultMap)
	aggregatedResults := toArray(&resultMap)
	bytes, err := json.Marshal(aggregatedResults)
	if err != nil {
		return http.StatusInternalServerError, nil
	}
	sendJSON(bytes, rw)

	return http.StatusOK, nil
}

//parse from a string into an array of agentid
func createFilter(query string) ([]serialization.ResultTypeFilter, error) {
	filters := make([]serialization.ResultTypeFilter, 0)
	for _, filterString := range strings.Split(query, ",") {
		parts := strings.SplitN(filterString, "|", 2)
		agentTypeID, err := strconv.Atoi(parts[0])
		if err != nil {
			return nil, err
		}
		filters = append(filters, serialization.ResultTypeFilter{AgentTypeID: agentTypeID, ResultTypeName: parts[1]})
	}
	return filters, nil
}

type result struct {
	err          error
	resultValues map[string]interface{}
	simtime      float64
}

//get results from cassandra and send it into the channel
func getResults(ctx context.Context, session *gocql.Session, simID int, start float64, stop float64, agentIDs []string, filterResultTypes []serialization.ResultTypeFilter, ch chan result) {
	q := `SELECT agentid, agenttypeid, simtime, result FROM results WHERE simid=? AND agentid IN ? AND simtime >= ? AND simtime <= ?`
	iter := session.Query(q, simID, agentIDs, start, stop).Iter()

	var dbAgentTypeID int
	var dbAgentID string
	var dbSimTime float64
	var dbSerialized []byte

	for iter.Scan(&dbAgentID, &dbAgentTypeID, &dbSimTime, &dbSerialized) {
		select {
		case <-ctx.Done(): // request canceled
			//send empty package
			ch <- result{}
			return
		default:
			//try to get agentType name
			var name string
			namePtr := serialization.GetAgentTypeName(dbAgentTypeID)
			if namePtr != nil {
				name = *namePtr
			}

			_, resultValues, err := serialization.DeserializeToMapWithResulttypes(dbAgentTypeID, dbSerialized, filterResultTypes)

			if err != nil {
				ch <- result{err: err}
				continue
			}
			// could not deserialize
			if resultValues == nil {
				log.Printf("Could not deserialize AgentTypeID %d %s", dbAgentTypeID, name)
				continue
			}
			ch <- result{resultValues: resultValues, simtime: dbSimTime}
		}
	}
	if err := iter.Close(); err != nil {
		ch <- result{err: err}
	}
	close(ch)
}

func addResultToMap(resultMap *map[float64]map[string]*aggregation, results *result) {
	if _, ok := (*resultMap)[results.simtime]; !ok {
		(*resultMap)[results.simtime] = make(map[string]*aggregation)
	}
	for resulttype, value := range results.resultValues {
		var vFloat64 float64
		switch v := value.(type) {
		case int:
			vFloat64 = float64(v)
			break
		case int32:
			vFloat64 = float64(v)
			break
		case int64:
			vFloat64 = float64(v)
			break
		case float32:
			vFloat64 = float64(v)
			break
		case uint64:
			vFloat64 = float64(v)
			break
		case uint32:
			vFloat64 = float64(v)
			break
		case uint:
			vFloat64 = float64(v)
			break
		case float64:
			vFloat64 = v
			break
		default:
			log.Printf("Invalid type: %T", v)
			log.Println("Value:", v)
			log.Println("Continuing with next value")
			continue
		}

		aggregate, ok := (*resultMap)[results.simtime][resulttype]
		if !ok {
			aggregate = newAggregate()
		}
		//check min
		if vFloat64 < aggregate.min {
			aggregate.min = vFloat64
		}
		//check max
		if vFloat64 > aggregate.max {
			aggregate.max = vFloat64
		}
		aggregate.sum += vFloat64
		aggregate.count++
		(*resultMap)[results.simtime][resulttype] = aggregate
	}
}

func newAggregate() *aggregation {
	var a aggregation
	a.avg = 0
	a.count = 0
	a.max = -math.MaxFloat64
	a.min = math.MaxFloat64
	a.sum = 0
	a.stddevsum = 0
	a.norm = 1
	return &a
}

func calcAverages(resultMap *map[float64]map[string]*aggregation) {
	for simtime, values := range *resultMap {
		for resulttypeid, result := range values {
			result.avg = result.sum / float64(result.count)
			(*resultMap)[simtime][resulttypeid] = result
		}
	}
}

func calcStddev(ctx context.Context, resultMap *map[float64]map[string]*aggregation, ch chan result) error {
	for r := range ch {
		select {
		case <-ctx.Done():
			return fmt.Errorf("Request canceled")
		default:
			if r.err != nil {
				return r.err
			}
			calcVarianceForResultMap(resultMap, &r)
		}
	}
	return nil
}

func calcVarianceForResultMap(resultMap *map[float64]map[string]*aggregation, r *result) {
	if _, ok := (*resultMap)[r.simtime]; !ok {
		log.Println("Could not find simtime in result map for variance calculation...something is wrong here")
		return
	}
	for resulttype, value := range r.resultValues {
		var vFloat64 float64
		switch value.(type) {
		case float64:
			vFloat64 = value.(float64)
			break
		case uint64:
			vFloat64 = float64(value.(uint64))
			break
		case uint32:
			vFloat64 = float64(value.(uint32))
			break
		case int32:
			vFloat64 = float64(value.(int32))
			break
		default:
			log.Printf("Could not find corresponding type to result: %T", value)
		}
		aggregate, ok := (*resultMap)[r.simtime][resulttype]
		if !ok {
			log.Println("Could not find resulttype in result map for variance calculation...something is wrong here")
			continue
		}
		vFloat64 /= aggregate.norm
		sum := (aggregate.avg - vFloat64) * (aggregate.avg - vFloat64)
		(*resultMap)[r.simtime][resulttype].stddevsum += sum
	}
}

type ArrayResulttype struct {
	Resulttype                 string
	Min, Max, Avg, Stddev, Sum float64
}

type ArrayAgent struct {
	Simtime     float64
	ResultTypes []ArrayResulttype
}

func toArray(resultMap *map[float64]map[string]*aggregation) *[]ArrayAgent {
	arr := make([]ArrayAgent, len(*resultMap))
	agentindex := 0
	for simtime, values := range *resultMap {
		var aa ArrayAgent
		aa.Simtime = simtime
		resulttypes := make([]ArrayResulttype, len((*resultMap)[simtime]))
		resultindex := 0
		for resulttypeid, result := range values {
			var ar ArrayResulttype
			ar.Resulttype = resulttypeid
			ar.Avg = result.avg
			ar.Max = result.max
			ar.Min = result.min
			ar.Stddev = math.Sqrt(result.stddevsum / float64(result.count))
			ar.Sum = result.sum
			resulttypes[resultindex] = ar
			resultindex++
		}
		aa.ResultTypes = resulttypes
		arr[agentindex] = aa
		agentindex++
	}
	return &arr
}

func sendJSON(bytes []byte, rw http.ResponseWriter) {
	rw.Header().Set("Content-Type", "application/json")
	_, err := rw.Write(bytes)
	if err != nil {
		log.Printf("Could not write bytes to response: %v", err)
	}
}
