// agents package
//
// @author Sonja Happ <sonja.happ@eonerc.rwth-aachen.de>, Jonas Otten
// @copyright 2019, Institute for Automation of Complex Power Systems, EONERC
// @license GNU General Public License (version 3)
//
// DistAIX Web
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

package agents

import (
	"encoding/json"
	"git.rwth-aachen.de/acs/public/DistAIXFramework/distaixweb.git/storage"
	"git.rwth-aachen.de/acs/public/DistAIXFramework/distaixweb.git/web"
	"log"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

type Agent struct {
	AgentTypeID   int               `json:"agentTypeID"`
	AgentTypeName string            `json:"agentTypeName"`
	AgentID       string            `json:"agentID"`
	Metadata      map[string]string `json:"metadata"`
}

func HandleAgents(ctx *web.Context, rw http.ResponseWriter, r *http.Request) (int, error) {
	vars := mux.Vars(r)
	simid, err := strconv.Atoi(vars["simulationid"])
	if err != nil {
		http.Error(rw, "Internal Server Error", http.StatusInternalServerError)
		log.Println("Could not parse simulationid")
		return http.StatusInternalServerError, nil
	}
	agentTypes, err := web.CreateIntArrayFromString(vars["agenttypes"])
	if err != nil {
		http.Error(rw, "Internal Server Error", http.StatusInternalServerError)
		log.Println("Could not parse agenttypes")
		return http.StatusInternalServerError, nil
	}
	agents, err := storage.GetAgentsFiltered(ctx.DBConn, simid, agentTypes)
	if err != nil {
		http.Error(rw, "Internal Server Error", http.StatusInternalServerError)
		log.Println("Could not get agentids", err)
		return http.StatusInternalServerError, nil
	}
	bytes, err := json.Marshal(&agents)
	if err != nil {
		http.Error(rw, "Internal Server Error", http.StatusInternalServerError)
		log.Println("Could not marshal agents", err)
		return http.StatusInternalServerError, nil
	}
	web.SendJSON(bytes, rw)
	return http.StatusOK, nil
}
