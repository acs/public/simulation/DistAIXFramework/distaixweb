// simulations package
//
// @author Sonja Happ <sonja.happ@eonerc.rwth-aachen.de>, Jonas Otten
// @copyright 2019, Institute for Automation of Complex Power Systems, EONERC
// @license GNU General Public License (version 3)
//
// DistAIX Web
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

package simulations

import (
	"encoding/json"
	"git.rwth-aachen.de/acs/public/DistAIXFramework/distaixweb.git/storage"
	"git.rwth-aachen.de/acs/public/DistAIXFramework/distaixweb.git/web"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

func HandleSimulations(ctx *web.Context, rw http.ResponseWriter, r *http.Request) (int, error) {
	simulations, err := storage.GetSimulations(ctx.DBConn)
	if err != nil {
		web.InternalError(rw, err)
		return http.StatusInternalServerError, nil
	}
	bytes, err := json.Marshal(simulations)
	web.SendJSON(bytes, rw)
	return http.StatusOK, nil
}

func HandleDeleteSimulations(ctx *web.Context, rw http.ResponseWriter, r *http.Request) (int, error) {
	vars := mux.Vars(r)
	simid, err := strconv.Atoi(vars["simulationid"])
	if err != nil {
		return http.StatusBadRequest, err
	}
	err = storage.DeleteSimulation(ctx.CassandraSession, ctx.DBConn, ctx.SidebarDBConn, simid)
	if err == storage.ErrNoAgentIDs {
		rw.Write([]byte(err.Error()))
		return http.StatusBadRequest, err
	}
	if err != nil {
		return http.StatusInternalServerError, err
	}
	return http.StatusOK, nil
}

func HandleGetSimulationTimes(ctx *web.Context, rw http.ResponseWriter, r *http.Request) (int, error) {
	vars := mux.Vars(r)
	simid, err := strconv.Atoi(vars["simulationid"])
	if err != nil {
		return http.StatusBadRequest, err
	}
	times, err := storage.GetTimes(ctx.DBConn, ctx.CassandraSession, simid)
	if err != nil {
		web.InternalError(rw, err)
		return http.StatusInternalServerError, nil
	}
	bytes, err := json.Marshal(times)
	web.SendJSON(bytes, rw)
	return http.StatusOK, nil
}
