// sidebar package
//
// @author Sonja Happ <sonja.happ@eonerc.rwth-aachen.de>, Jonas Otten
// @copyright 2019, Institute for Automation of Complex Power Systems, EONERC
// @license GNU General Public License (version 3)
//
// DistAIX Web
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

package sidebar

import (
	"encoding/json"
	"git.rwth-aachen.de/acs/public/DistAIXFramework/distaixweb.git/storage"
	"git.rwth-aachen.de/acs/public/DistAIXFramework/distaixweb.git/web"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

func HandleGetSidebar(ctx *web.Context, w http.ResponseWriter, r *http.Request) (int, error) {
	elements, err := storage.SidebarGetElements(ctx.SidebarDBConn)
	if err != nil {
		return http.StatusInternalServerError, err
	}

	bytes, err := json.Marshal(elements)
	if err != nil {
		return http.StatusInternalServerError, err
	}
	web.SendJSON(bytes, w)
	return http.StatusOK, nil
}

func HandlePostSidebar(ctx *web.Context, w http.ResponseWriter, r *http.Request) (int, error) {
	var jsonElement storage.SidebarElement
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return http.StatusInternalServerError, err
	}
	json.Unmarshal(body, &jsonElement)
	//check if all set
	if jsonElement.Name == "" ||
		jsonElement.AgentIDs == "" ||
		(jsonElement.ResultTypesLeft == "" && jsonElement.ResultTypesRight == "") ||
		jsonElement.SimulationID < 1 {
		return http.StatusBadRequest, nil
	}
	err = storage.SidebarAddElement(ctx.SidebarDBConn, jsonElement)
	if err != nil {
		http.Error(w, "Element name already exists", http.StatusConflict)
		return http.StatusConflict, nil
	}
	return http.StatusOK, nil
}

func HandleDeleteSidebar(ctx *web.Context, w http.ResponseWriter, r *http.Request) (int, error) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		return http.StatusInternalServerError, err
	}
	log.Println("Delete Sidebar element with id", id)
	if err := storage.SidebarDeleteElement(ctx.SidebarDBConn, id); err != nil {
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
	}
	return http.StatusOK, nil
}
