// results package
//
// @author Sonja Happ <sonja.happ@eonerc.rwth-aachen.de>, Jonas Otten
// @copyright 2019, Institute for Automation of Complex Power Systems, EONERC
// @license GNU General Public License (version 3)
//
// DistAIX Web
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

package results

import (
	"database/sql"
	"git.rwth-aachen.de/acs/public/DistAIXFramework/distaixweb.git/storage"
	"git.rwth-aachen.de/acs/public/DistAIXFramework/distaixweb.git/storage/serialization"
	"log"
	"strconv"
	"strings"

	"github.com/gocql/gocql"
)

func getResultsWithoutLimits(vars map[string]string, db *sql.DB, session *gocql.Session, resultTypeFilter []serialization.ResultTypeFilter) ([]storage.AgentResult, error) {
	simid, err := strconv.Atoi(vars["simulationid"])
	if err != nil {
		log.Print("Could not parse simulationid")
		return nil, err
	}
	agents := strings.Split(vars["agents"], ",")
	if err != nil {
		log.Println("Could not parse agenttypes")
		return nil, err
	}
	results, err := storage.GetResultsNoLimits(db, session, simid, agents, resultTypeFilter)
	if err != nil {
		return nil, err
	}
	return results, nil
}
