// results package
//
// @author Sonja Happ <sonja.happ@eonerc.rwth-aachen.de>, Jonas Otten
// @copyright 2019, Institute for Automation of Complex Power Systems, EONERC
// @license GNU General Public License (version 3)
//
// DistAIX Web
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

package results

import (
	"encoding/csv"
	"encoding/json"
	"fmt"
	"git.rwth-aachen.de/acs/public/DistAIXFramework/distaixweb.git/handlers/aggregatePerAgent"
	"git.rwth-aachen.de/acs/public/DistAIXFramework/distaixweb.git/handlers/aggregatePerTimeStep"
	"git.rwth-aachen.de/acs/public/DistAIXFramework/distaixweb.git/storage"
	"git.rwth-aachen.de/acs/public/DistAIXFramework/distaixweb.git/storage/serialization"
	"git.rwth-aachen.de/acs/public/DistAIXFramework/distaixweb.git/web"
	"log"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

//handleResultWithResulttypes handles requests for results for agentid|resulttype pairs
func HandleResultWithResulttypes(ctx *web.Context, rw http.ResponseWriter, r *http.Request) (int, error) {
	vars := mux.Vars(r)

	//create filters
	filters, err := web.CreateFilter(vars["resulttypes"])
	if err != nil {
		http.Error(rw, "Internal Server Error", http.StatusInternalServerError)
		log.Printf("Could create from query %s filters: %v", vars["resulttypes"], err)
		return http.StatusInternalServerError, err
	}

	params, err := parseParams(vars)
	if err != nil {
		return http.StatusInternalServerError, err
	}
	agents, err := getAgentsIfNeeded(ctx, params.simid, filters, params.agents)
	if err != nil {
		return http.StatusInternalServerError, err
	}
	results, err := storage.GetResults(ctx.DBConn, ctx.CassandraSession, params.simid, agents, params.start, params.stop, filters, -1)
	if err != nil {
		http.Error(rw, "Internal Server Error", http.StatusInternalServerError)
		log.Println("Could not get results:", err)
		return http.StatusInternalServerError, err
	}

	bytes, err := json.Marshal(&results)
	if err != nil {
		web.InternalError(rw, err)
		return http.StatusInternalServerError, nil
	}
	web.SendJSON(bytes, rw)

	return http.StatusOK, nil
}

//handleResultWithResulttypes handles requests for results for agentid|resulttype pairs without specific start/stop times
func HandleResultWithResulttypesNoLimits(ctx *web.Context, rw http.ResponseWriter, r *http.Request) (int, error) {
	vars := mux.Vars(r)

	filters, err := web.CreateFilter(vars["resulttypes"])
	if err != nil {
		http.Error(rw, "Internal Server Error", http.StatusInternalServerError)
		log.Printf("Could create from query %s filters: %v", vars["resulttypes"], err)
		return http.StatusInternalServerError, err
	}

	results, err := getResultsWithoutLimits(vars, ctx.DBConn, ctx.CassandraSession, filters)
	if err != nil {
		http.Error(rw, "Internal Server Error", http.StatusInternalServerError)
		log.Println("Could not get results:", err)
		return http.StatusInternalServerError, err
	}

	bytes, err := json.Marshal(&results)
	if err != nil {
		web.InternalError(rw, err)
		return http.StatusInternalServerError, nil
	}
	web.SendJSON(bytes, rw)

	return http.StatusOK, nil
}

func HandleAgentAggregation(ctx *web.Context, rw http.ResponseWriter, r *http.Request) (int, error) {
	return aggregatePerAgent.Handle(ctx.CassandraSession, ctx.DBConn, rw, r)
}
func HandleAgentAggregationCSV(ctx *web.Context, rw http.ResponseWriter, r *http.Request) (int, error) {
	return aggregatePerAgent.HandleCSV(ctx.CassandraSession, ctx.DBConn, rw, r)
}

func HandleTimeAggregation(ctx *web.Context, rw http.ResponseWriter, r *http.Request) (int, error) {
	return aggregatePerTimestep.Handle(ctx.CassandraSession, ctx.DBConn, rw, r)
}
func HandleTimeAggregationCSV(ctx *web.Context, rw http.ResponseWriter, r *http.Request) (int, error) {
	return aggregatePerTimestep.HandleCSV(ctx.CassandraSession, ctx.DBConn, rw, r)
}

func HandleResultWithResulttypesCSV(ctx *web.Context, rw http.ResponseWriter, r *http.Request) (int, error) {
	vars := mux.Vars(r)

	filters, err := web.CreateFilter(vars["resulttypes"])
	if err != nil {
		http.Error(rw, "Internal Server Error", http.StatusInternalServerError)
		log.Printf("Could create from query %s filters: %v", vars["resulttypes"], err)
		return http.StatusInternalServerError, err
	}

	params, err := parseParams(vars)
	if err != nil {
		return http.StatusInternalServerError, err
	}

	agents, err := getAgentsIfNeeded(ctx, params.simid, filters, params.agents)
	if err != nil {
		return http.StatusInternalServerError, err
	}
	results, err := storage.GetResults(ctx.DBConn, ctx.CassandraSession, params.simid, agents, params.start, params.stop, filters, -1)
	//results, err := getResults(vars, ctx.CassandraSession, filters)
	if err != nil {
		http.Error(rw, "Internal Server Error", http.StatusInternalServerError)
		log.Println("Could not get results:", err)
		return http.StatusInternalServerError, err
	}

	csvlines := make([][]string, 0)
	header := make([]string, 0)
	header = append(header, "simtime")
	nameIndizes := make(map[string]int)

	// determine all different resulltypes/agent combinations and specify an index for them
	index := 1 // one because simtime is zero
	for _, result := range results {
		for _, value := range result.Values {
			key := web.GenerateKey(value.AgentTypeName, value.ResultTypeName, value.AgentID)
			if _, ok := nameIndizes[key]; !ok {
				header = append(header, key)
				nameIndizes[key] = index
				index++
			}
		}
	}

	//append it
	csvlines = append(csvlines, header)

	//go through all results and set the value at the corresponding position in the line array
	for _, result := range results {
		line := make([]string, len(header))
		line[0] = fmt.Sprint(result.SimTime)
		for _, value := range result.Values {
			key := web.GenerateKey(value.AgentTypeName, value.ResultTypeName, value.AgentID)
			i := nameIndizes[key]
			switch value.Value.(type) {
			case float64:
				line[i] = strconv.FormatFloat(value.Value.(float64), 'f', -1, 64)
				break
			case float32:
				line[i] = strconv.FormatFloat(value.Value.(float64), 'f', -1, 32)
				break
			case int:
				line[i] = fmt.Sprint(value.Value)
				break
			case int32:
				line[i] = fmt.Sprint(value.Value)
				break
			case uint64:
				line[i] = fmt.Sprint(value.Value)
				break
			case uint32:
				line[i] = fmt.Sprint(value.Value)
				break
			default:
				log.Printf("[CSV] Couldn't find a case for type %T\nusing fmt.Sprint as a default", value.Value)
				line[i] = fmt.Sprint(value.Value)

			}
		}
		csvlines = append(csvlines, line)
	}

	cw := csv.NewWriter(rw)
	rw.Header().Add("Content-Disposition", "attachment")
	rw.Header().Add("Content-Type", "text/csv")
	err = cw.WriteAll(csvlines)
	if err != nil {
		http.Error(rw, "Internal Server Error", http.StatusInternalServerError)
		log.Println("Could not write csv: ", err)
		return 0, nil
	}
	return http.StatusOK, nil
}

// get AgentIDs for specific filters(agenttypeIDs)
func getAgentsIfNeeded(ctx *web.Context, simid int, filters []serialization.ResultTypeFilter, agents []string) ([]string, error) {
	if agents != nil {
		return agents, nil
	}
	agenttypes := make([]int, 0)
	for _, a := range filters {
		agenttypes = append(agenttypes, a.AgentTypeID)
	}
	storageAgents, err := storage.GetAgentsFiltered(ctx.DBConn, simid, agenttypes)
	if err != nil {
		return nil, err
	}
	agents = make([]string, 0)
	for _, agenttype := range storageAgents {
		for _, agent := range agenttype.Agents {
			agents = append(agents, agent.AgentID)
		}
	}
	return agents, nil
}
