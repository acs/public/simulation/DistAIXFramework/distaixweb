// results package
//
// @author Sonja Happ <sonja.happ@eonerc.rwth-aachen.de>, Jonas Otten
// @copyright 2019, Institute for Automation of Complex Power Systems, EONERC
// @license GNU General Public License (version 3)
//
// DistAIX Web
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

package results

import (
	"log"
	"strconv"
	"strings"
)

type params struct {
	simid       int
	start, stop float64
	agents      []string
}

func parseParams(vars map[string]string) (*params, error) {
	var p params
	var err error
	p.simid, err = strconv.Atoi(vars["simulationid"])
	if err != nil {
		log.Print("Could not parse simulationid")
		return nil, err
	}
	if v, ok := vars["agents"]; ok {
		p.agents = strings.Split(v, ",")
	} else {
		p.agents = nil
	}

	p.start, err = strconv.ParseFloat(vars["start"], 64)
	if err != nil {
		log.Print("Could not parse start")
		return nil, err
	}
	p.stop, err = strconv.ParseFloat(vars["stop"], 64)
	if err != nil {
		log.Print("Could not parse stop")
		return nil, err
	}
	return &p, nil
}
