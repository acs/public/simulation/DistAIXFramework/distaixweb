// main test package
//
// @author Sonja Happ <sonja.happ@eonerc.rwth-aachen.de>, Jonas Otten
// @copyright 2019, Institute for Automation of Complex Power Systems, EONERC
// @license GNU General Public License (version 3)
//
// DistAIX Web
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

package main

import (
	"log"
	"net/http"
	"time"
)

const host = "http://127.0.0.1"

func main() {
	test("/api/v1/simulations")
	test("/api/v1/simulations/1/agenttypes")
	test("/api/v1/simulations/1/agents?agenttypes=0")
}

func logResult(statusCode int, seconds float64, part string) {
	status := "FAIL"
	if statusCode == http.StatusOK {
		status = "OK"
	}
	log.Printf("[%s] %s time: %f status: %d", status, part, seconds, statusCode)
}

func test(uri string) {
	start := time.Now()
	resp, err := http.Get(host + uri)
	if err != nil {
		log.Fatal(err)
	}
	duration := time.Since(start).Seconds()
	logResult(resp.StatusCode, duration, uri)
}
