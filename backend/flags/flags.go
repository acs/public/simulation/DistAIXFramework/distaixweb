// flags package
//
// @author Sonja Happ <sonja.happ@eonerc.rwth-aachen.de>, Jonas Otten
// @copyright 2019, Institute for Automation of Complex Power Systems, EONERC
// @license GNU General Public License (version 3)
//
// DistAIX Web
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

package flags

import (
	"flag"
	"log"
	"os"
	"sort"
	"strings"

	go_config "github.com/zpatrick/go-config"
)

var GlobalConfig *go_config.Config = nil

func InitConfig() error {

	if GlobalConfig != nil {
		return nil
	}

	var (
		port       = flag.String("port", "4000", "Port on which the application should listen on")
		apiPath    = flag.String("apipath", "api/index.html", "Path to html file for API doc, shown at /api")
		configFile = flag.String("config", "", "Path to YAML configuration file")

		postgresHost = flag.String("phost", "127.0.0.1", "Postgres DB host")
		postgresUser = flag.String("puser", "postgres", "Postgres DB user")
		postgresPW   = flag.String("ppass", "postgres", "Postgres DB password")
		postgresDB   = flag.String("pdatabase", "swarmgrid", "Postgres DB name")
		postgresPort = flag.String("pport", "5432", "Postgres port")

		cassandraHosts = flag.String("casshosts", "127.0.0.1", "comma seperated list of cassandra hosts")
		cassandraDB    = flag.String("cassdb", "swarmgrid", "Cassandra keyspace name")
	)
	flag.Parse()

	static := map[string]string{
		"port":        *port,
		"apipath":     *apiPath,
		"config.file": *configFile,

		"postgres.host":     *postgresHost,
		"postgres.user":     *postgresUser,
		"postgres.password": *postgresPW,
		"postgres.db":       *postgresDB,
		"postgres.port":     *postgresPort,

		"cassandra.hosts": *cassandraHosts,
		"cassandra.db":    *cassandraDB,
	}

	mappings := map[string]string{}
	for name := range static {
		envName := strings.ReplaceAll(name, ".", "_")
		envName = strings.ReplaceAll(envName, "-", "_")
		mappings[envName] = name
	}

	defaults := go_config.NewStatic(static)
	env := go_config.NewEnvironment(mappings)

	if _, err := os.Stat(*configFile); os.IsNotExist(err) {
		GlobalConfig = go_config.NewConfig([]go_config.Provider{defaults, env})
	} else {
		yamlFile := go_config.NewYAMLFile(*configFile)
		GlobalConfig = go_config.NewConfig([]go_config.Provider{defaults, yamlFile, env})
	}

	err := GlobalConfig.Load()
	if err != nil {
		log.Fatal("failed to parse config")
		return err
	}

	settings, _ := GlobalConfig.Settings()

	keys := make([]string, 0, len(settings))
	for k := range settings {
		keys = append(keys, k)
	}
	sort.Strings(keys)

	log.Print("All settings (except for PW and secrets):")
	for _, k := range keys {
		if !strings.Contains(k, "pass") && !strings.Contains(k, "secret") {
			log.Printf("   %s = %s \n", k, settings[k])
		}
	}
	return nil

}
