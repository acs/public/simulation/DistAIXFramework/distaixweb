module git.rwth-aachen.de/acs/public/DistAIXFramework/distaixweb.git

go 1.16

require (
	github.com/BurntSushi/toml v1.0.0 // indirect
	github.com/ghodss/yaml v1.0.0 // indirect
	github.com/go-ini/ini v1.66.4 // indirect
	github.com/gocql/gocql v1.0.0
	github.com/golang/protobuf v1.5.2
	github.com/golang/snappy v0.0.4 // indirect
	github.com/gorilla/handlers v1.5.1
	github.com/gorilla/mux v1.8.0
	github.com/lib/pq v1.10.4
	github.com/urfave/cli v1.22.5 // indirect
	github.com/zpatrick/go-config v0.0.0-20191118215128-80ba6b3e54f6
	google.golang.org/protobuf v1.27.1
)
