// main package
//
// @author Sonja Happ <sonja.happ@eonerc.rwth-aachen.de>, Jonas Otten
// @copyright 2019, Institute for Automation of Complex Power Systems, EONERC
// @license GNU General Public License (version 3)
//
// DistAIX Web
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

package main

import (
	"git.rwth-aachen.de/acs/public/DistAIXFramework/distaixweb.git/flags"
	"git.rwth-aachen.de/acs/public/DistAIXFramework/distaixweb.git/storage"
	"git.rwth-aachen.de/acs/public/DistAIXFramework/distaixweb.git/web"
	webconfig "git.rwth-aachen.de/acs/public/DistAIXFramework/distaixweb.git/web-config"
	"log"
	"strings"
	"sync"
	"time"

	"github.com/gocql/gocql"
)

// CassTimeout is the timeout for the cassandra cluster
const CassTimeout = time.Minute

func main() {
	err := flags.InitConfig()
	if err != nil {
		log.Fatalf("Could not initi config: %v", err)
	}

	apiPath, err := flags.GlobalConfig.String("apipath")
	if err != nil {
		log.Fatalf("Parameter API path missing: %v", err)
	}
	port, err := flags.GlobalConfig.String("port")
	if err != nil {
		log.Fatalf("Parameter port missing: %v", err)
	}
	postgresUser, err := flags.GlobalConfig.String("postgres.user")
	if err != nil {
		log.Fatalf("Parameter postgres user missing: %v", err)
	}
	postgresPassword, err := flags.GlobalConfig.String("postgres.password")
	if err != nil {
		log.Fatalf("Parameter postgres password missing: %v", err)
	}
	postgresHost, err := flags.GlobalConfig.String("postgres.host")
	if err != nil {
		log.Fatalf("Parameter postgres host missing: %v", err)
	}
	postgresPort, err := flags.GlobalConfig.String("postgres.port")
	if err != nil {
		log.Fatalf("Parameter postgres port missing: %v", err)
	}
	postgresDB, err := flags.GlobalConfig.String("postgres.db")
	if err != nil {
		log.Fatalf("Parameter postgres db missing: %v", err)
	}
	cassandraHosts, err := flags.GlobalConfig.String("cassandra.hosts")
	if err != nil {
		log.Fatalf("Parameter cassandra hosts missing: %v", err)
	}
	cassandraDB, err := flags.GlobalConfig.String("cassandra.db")
	if err != nil {
		log.Fatalf("Parameter cassandra db missing: %v", err)
	}

	db, err := storage.Open(postgresUser, postgresPassword, postgresHost, postgresPort, postgresDB)
	if err != nil {
		log.Fatalf("Could not open DB: %v", err)
	}
	if err := db.Ping(); err != nil {
		log.Fatalf("Could not reach DB: %v", err)
	}
	sidebardb, err := storage.OpenSidebarConn(postgresUser, postgresPassword, postgresHost, postgresPort, postgresDB)
	if err != nil {
		log.Fatalf("Could not open Sidebar DB: %v", err)
	}
	if err := sidebardb.Ping(); err != nil {
		log.Fatalf("Could not reach Sidebar DB: %v", err)
	}
	cluster := gocql.NewCluster(strings.Split(cassandraHosts, ",")...)
	cluster.Keyspace = cassandraDB
	cluster.Timeout = CassTimeout

	session, err := cluster.CreateSession()
	if err != nil {
		log.Fatal("Cloud not create Cassandra Session", err)
	}
	m := make(map[string]web.NodeStatusValue)
	mtx := &sync.Mutex{} //used for node status map
	ctx := web.Context{DBConn: db, SidebarDBConn: sidebardb, CassandraSession: session, NodeStatus: m, Mtx: mtx}
	webconfig.Start(apiPath, port, &ctx)
}
