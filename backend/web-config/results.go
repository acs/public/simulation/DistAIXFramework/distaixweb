// webconfig package
//
// @author Sonja Happ <sonja.happ@eonerc.rwth-aachen.de>, Jonas Otten
// @copyright 2019, Institute for Automation of Complex Power Systems, EONERC
// @license GNU General Public License (version 3)
//
// DistAIX Web
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

package webconfig

import (
	"git.rwth-aachen.de/acs/public/DistAIXFramework/distaixweb.git/handlers/results"
	"git.rwth-aachen.de/acs/public/DistAIXFramework/distaixweb.git/web"

	"github.com/gorilla/mux"
)

// Note: Since the timemeasurements are used as normal results, the resulttypes are not as restrictive as before (the regex accepts everything besides a comma, or to put it in another way: everything)
// That is not well tested, but I don't think there will be any issues with that
// If there are issues, you might want to reuse the old regex:
// Queries("resulttypes", "{resulttypes:[-]?[0-9]+\\|[0-9A-Za-z\\_]+(?:,[-]?[0-9]+\\|[0-9A-Za-z\\_]+)*}").

func registerResults(r *mux.Router, c *web.Context) {
	//WARNING: the order of the endpoint registration is important, since gorilla mux will use the first matching endpoint, therefore a more specialised endpoint needs to get registered first

	//default result endpoint
	r.Handle("/api/v1/simulations/{simulationid:[0-9]+}/results/filtered", Handler{c, results.HandleResultWithResulttypes}).Methods("GET").
		Queries("agents", "{agents:[^,]+(?:,[^,]+)*}").
		Queries("resulttypes", "{resulttypes:[^,]+(?:,[^,]+)*}").
		Queries("start", "{start:[0-9]+\\.?[0-9]*}").
		Queries("stop", "{stop:[0-9]+\\.?[0-9]*}")

	//preview for agent creation (show the last 30 timesteps)
	r.Handle("/api/v1/simulations/{simulationid:[0-9]+}/results/filtered", Handler{c, results.HandleResultWithResulttypesNoLimits}).Methods("GET").
		Queries("agents", "{agents:[^,]+(?:,[^,]+)*}").
		Queries("resulttypes", "{resulttypes:[^,]+(?:,[^,]+)*}")

	//preview for agent creation (show the last 30 timesteps)
	r.Handle("/api/v1/simulations/{simulationid:[0-9]+}/results/filtered", Handler{c, results.HandleResultWithResulttypesNoLimits}).Methods("GET").
		Queries("agents", "{agents:[^,]+(?:,[^,]+)*}").
		Queries("resulttypes", "{resulttypes:[^,]+(?:,[^,]+)*}")

	//csv endpoint
	r.Handle("/api/v1/simulations/{simulationid:[0-9]+}/results/filtered/results.csv", Handler{c, results.HandleResultWithResulttypesCSV}).Methods("GET").
		Queries("agents", "{agents:[^,]+(?:,[^,]+)*}").
		Queries("resulttypes", "{resulttypes:[^,]+(?:,[^,]+)*}").
		Queries("start", "{start:[0-9]+\\.?[0-9]*}").
		Queries("stop", "{stop:[0-9]+\\.?[0-9]*}")

	/********************************************************/
	/*					PREPARED RESULTS					*/
	/********************************************************/

	// Note: Currently the JSON endpoints are not in use

	//per agent aggregation (json)
	r.Handle("/api/v1/simulations/{simulationid:[0-9]+}/customresults/per-agent-aggregation", Handler{c, results.HandleAgentAggregation}).Methods("GET").
		Queries("resulttypes", "{resulttypes:[^,]+(?:,[^,]+)*}").
		Queries("start", "{start:[0-9]+\\.?[0-9]*}").
		Queries("stop", "{stop:[0-9]+\\.?[0-9]*}")

	//per agent aggregation (csv)
	r.Handle("/api/v1/simulations/{simulationid:[0-9]+}/customresults/per-agent-aggregation.csv", Handler{c, results.HandleAgentAggregationCSV}).Methods("GET").
		Queries("resulttypes", "{resulttypes:[^,]+(?:,[^,]+)*}").
		Queries("start", "{start:[0-9]+\\.?[0-9]*}").
		Queries("stop", "{stop:[0-9]+\\.?[0-9]*}")

	//per timestep aggregation (json)
	r.Handle("/api/v1/simulations/{simulationid:[0-9]+}/customresults/per-timestep-aggregation", Handler{c, results.HandleTimeAggregation}).Methods("GET").
		Queries("resulttypes", "{resulttypes:[^,]+(?:,[^,]+)*}").
		Queries("start", "{start:[0-9]+\\.?[0-9]*}").
		Queries("stop", "{stop:[0-9]+\\.?[0-9]*}")

	//per timestep aggregation (csv)
	r.Handle("/api/v1/simulations/{simulationid:[0-9]+}/customresults/per-timestep-aggregation.csv", Handler{c, results.HandleTimeAggregationCSV}).Methods("GET").
		Queries("resulttypes", "{resulttypes:[^,]+(?:,[^,]+)*}").
		Queries("start", "{start:[0-9]+\\.?[0-9]*}").
		Queries("stop", "{stop:[0-9]+\\.?[0-9]*}")
}
