// webconfig package
//
// @author Sonja Happ <sonja.happ@eonerc.rwth-aachen.de>, Jonas Otten
// @copyright 2019, Institute for Automation of Complex Power Systems, EONERC
// @license GNU General Public License (version 3)
//
// DistAIX Web
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

package webconfig

import (
	"fmt"
	"net/http"
	"os"
	"sync"

	"git.rwth-aachen.de/acs/public/DistAIXFramework/distaixweb.git/handlers/agents"
	"git.rwth-aachen.de/acs/public/DistAIXFramework/distaixweb.git/handlers/agenttypes"
	"git.rwth-aachen.de/acs/public/DistAIXFramework/distaixweb.git/handlers/nodestatus"
	"git.rwth-aachen.de/acs/public/DistAIXFramework/distaixweb.git/handlers/sidebar"
	"git.rwth-aachen.de/acs/public/DistAIXFramework/distaixweb.git/handlers/simulations"
	"git.rwth-aachen.de/acs/public/DistAIXFramework/distaixweb.git/web"
	"io/ioutil"
	"log"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
)

// Handler handles webhandlers with additional context
type Handler struct {
	*web.Context
	Handle func(*web.Context, http.ResponseWriter, *http.Request) (int, error)
}

func (wh Handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var wg sync.WaitGroup // create a wait group and wait until the thread is done
	wg.Add(1)
	go func() {
		status, err := wh.Handle(wh.Context, w, r)
		if err != nil {
			log.Printf("HTTP %d: %q", status, err)
			switch status {
			case http.StatusNotFound:
				http.NotFound(w, r)
			case http.StatusInternalServerError:
				http.Error(w, http.StatusText(status), status)
			default:
				http.Error(w, http.StatusText(status), status)
			}
		}
		wg.Done()
	}()
	wg.Wait()
}

//Start registers all routes
func Start(apiPath string, port string, c *web.Context) {
	r := mux.NewRouter()

	// show API documentation
	r.HandleFunc("/api", func(rw http.ResponseWriter, r *http.Request) {
		content, err := ioutil.ReadFile(apiPath)
		if err != nil {
			http.NotFound(rw, r)
			log.Printf("Could not find %s", apiPath)
			return
		}
		rw.Header().Add("Content-Type", "text/html")
		rw.Write(content)
	}).Methods("GET")

	registerResults(r, c)

	r.Handle("/api/v1/simulations", Handler{c, simulations.HandleSimulations}).Methods("GET")                                    //show simulations
	r.Handle("/api/v1/simulations/{simulationid:[0-9]+}", Handler{c, simulations.HandleDeleteSimulations}).Methods("DELETE")     //delete a simulation
	r.Handle("/api/v1/simulations/{simulationid:[0-9]+}/agenttypes", Handler{c, agenttypes.HandleAgentTypes}).Methods("GET")     //show agenttypes, and for each agenttypes resulttypes for simulation
	r.Handle("/api/v1/simulations/{simulationid:[0-9]+}/times", Handler{c, simulations.HandleGetSimulationTimes}).Methods("GET") //show start/stop times for a simulation
	r.Handle("/api/v1/simulations/{simulationid:[0-9]+}/agents", Handler{c, agents.HandleAgents}).Methods("GET").                //get agents for specific agenttypes for a simulation
																	Queries("agenttypes", "{agenttypes:[-]?[0-9]+(?:,[-]?[0-9]+)*}")

	r.Handle("/api/v1/sidebar", Handler{c, sidebar.HandleGetSidebar}).Methods("GET")                   //show all available graphs in the sidebar
	r.Handle("/api/v1/sidebar", Handler{c, sidebar.HandlePostSidebar}).Methods("POST")                 //create a new graph in the sidebar
	r.Handle("/api/v1/sidebar/{id:[0-9]+}", Handler{c, sidebar.HandleDeleteSidebar}).Methods("DELETE") //delete a graph from the sidebar

	r.Handle("/api/v1/nodestatus", Handler{c, nodestatus.HandleNodeStatusGet}).Methods("GET")                 //get all node status
	r.Handle("/api/v1/nodestatus/{nodename:.+}", Handler{c, nodestatus.HandleNodeStatusPost}).Methods("POST") //create a new status for nodename

	//r.PathPrefix("/").Handler(http.StripPrefix("/", http.FileServer(http.Dir(flags.FrontendPath))))
	http.Handle("/", r)
	log.Printf("Server running on :%s", port)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", port), handlers.LoggingHandler(os.Stdout, http.DefaultServeMux)))
}
