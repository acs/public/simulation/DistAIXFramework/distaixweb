FROM golang:1.16-buster AS builder

RUN mkdir /build
WORKDIR /build

# Make use of layer caching
ADD go.* ./
RUN go mod download
ADD . .

RUN apt-get update && \
    apt-get install -y  \
    protobuf-compiler  \
    ca-certificates \
    default-jre
RUN go install github.com/golang/protobuf/protoc-gen-go

# Generate go packaged from proto files
RUN protoc --proto_path=agents_proto_files --go_out=agents_protobuf --go_opt=paths=source_relative agents_proto_files/*.proto
RUN go build -o distaixweb-backend

# Generate html for API docs using swagger-codegen
RUN wget -q https://repo1.maven.org/maven2/io/swagger/swagger-codegen-cli/2.3.1/swagger-codegen-cli-2.3.1.jar -O swagger-codegen-cli.jar
RUN java -jar swagger-codegen-cli.jar generate -i ./api/api.yaml -l html2 -o ./api/

FROM debian:buster
COPY --from=builder /build/distaixweb-backend /usr/bin
EXPOSE 3000
CMD [ "distaixweb-backend" ]
