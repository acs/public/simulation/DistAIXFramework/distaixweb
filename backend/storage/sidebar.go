// storage package
//
// @author Sonja Happ <sonja.happ@eonerc.rwth-aachen.de>, Jonas Otten
// @copyright 2019, Institute for Automation of Complex Power Systems, EONERC
// @license GNU General Public License (version 3)
//
// DistAIX Web
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

package storage

import (
	"database/sql"
	"fmt"
)

func SidebarAddElement(db *sql.DB, sidebarElement SidebarElement) error {
	dbstr := fmt.Sprintf("INSERT INTO %s(name, simid, resulttypes_left, resulttypes_right, agentids) VALUES($1, $2, $3, $4, $5)", SIDEBARDB)
	err := db.QueryRow(dbstr, sidebarElement.Name, sidebarElement.SimulationID, sidebarElement.ResultTypesLeft, sidebarElement.ResultTypesRight, sidebarElement.AgentIDs).Scan()
	if err != nil && err != sql.ErrNoRows {
		return err
	}
	return nil
}

type SidebarElement struct {
	Id               int    `json:"id"`
	Name             string `json:"name"`
	SimulationID     int    `json:"simid"`
	ResultTypesLeft  string `json:"resulttypes_left"`
	ResultTypesRight string `json:"resulttypes_right"`
	AgentIDs         string `json:"agentids"`
}

func SidebarGetElements(db *sql.DB) ([]SidebarElement, error) {
	dbstr := fmt.Sprintf("SELECT sidebarid, name, simid, resulttypes_left, resulttypes_right, agentids FROM %s", SIDEBARDB)
	rows, err := db.Query(dbstr)
	if err != nil {
		return nil, err
	}
	elements := make([]SidebarElement, 0)
	for rows.Next() {
		var el SidebarElement
		if err := rows.Scan(&el.Id, &el.Name, &el.SimulationID, &el.ResultTypesLeft, &el.ResultTypesRight, &el.AgentIDs); err != nil {
			return nil, err
		}
		elements = append(elements, el)
	}
	return elements, nil
}

func SidebarDeleteElement(db *sql.DB, id int) error {
	dbstr := fmt.Sprintf("DELETE FROM %s WHERE sidebarid=$1", SIDEBARDB)
	if err := db.QueryRow(dbstr, id).Scan(); err != nil && err != sql.ErrNoRows {
		return err
	}
	return nil
}
