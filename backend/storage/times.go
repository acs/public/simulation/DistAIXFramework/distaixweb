// storage package
//
// @author Sonja Happ <sonja.happ@eonerc.rwth-aachen.de>, Jonas Otten
// @copyright 2019, Institute for Automation of Complex Power Systems, EONERC
// @license GNU General Public License (version 3)
//
// DistAIX Web
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

package storage

import (
	"database/sql"
	"log"

	"github.com/gocql/gocql"
)

type TimeResults struct {
	StartTime float64 `json:"starttime"`
	StopTime  float64 `json:"stoptime"`
}

const agentID = "1"

func GetTimes(db *sql.DB, session *gocql.Session, simID int) (*TimeResults, error) {
	q := session.Query(`SELECT simtime FROM results WHERE simid=? AND agentid=?`, simID, agentID)
	iter := q.Iter()

	var starttime, stoptime, simtime float64
	first := true
	for iter.Scan(&simtime) {
		if first {
			first = false
			starttime = simtime
		}
		stoptime = simtime
	}
	if err := iter.Close(); err != nil && err != gocql.ErrNotFound {
		log.Print("Could not get times", err)
		return nil, err
	}
	return &TimeResults{StartTime: starttime, StopTime: stoptime}, nil
}
