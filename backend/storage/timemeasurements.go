// storage package
//
// @author Sonja Happ <sonja.happ@eonerc.rwth-aachen.de>, Jonas Otten
// @copyright 2019, Institute for Automation of Complex Power Systems, EONERC
// @license GNU General Public License (version 3)
//
// DistAIX Web
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

package storage

import (
	"database/sql"
	"fmt"
	"git.rwth-aachen.de/acs/public/DistAIXFramework/distaixweb.git/storage/serialization"
	"log"
	"strconv"
	"strings"

	"github.com/gocql/gocql"

	"github.com/lib/pq"
)

// TimeMeasurementsAgentTypeID is a fake agenttype id
// to be able to show timemeasurements in the frontend
const TimeMeasurementsAgentTypeID = -4

// TimeMeasurementAgentTypeName is the name for the fake agenttype TimeMeasurement
const TimeMeasurementAgentTypeName = "TimeMeasurement"

func GetTimeMeasurementAgentType(db *sql.DB, simid int) (*serialization.AgentType, error) {
	rows, err := db.Query(`SELECT timemeasurementtype FROM timemeasurements WHERE simulationid=$1 GROUP BY timemeasurementtype`, simid)
	if err != nil && err != gocql.ErrNotFound {
		return nil, err
	}
	//ranks := make(map[int64][]string)
	resultTypes := make([]string, 0)
	for rows.Next() {
		var timeMeasurementType string
		if err := rows.Scan(&timeMeasurementType); err != nil {
			return nil, err
		}
		resultTypes = append(resultTypes, timeMeasurementType)
	}
	var agentType serialization.AgentType
	agentType.ID = TimeMeasurementsAgentTypeID
	agentType.Type = TimeMeasurementAgentTypeName
	agentType.ResultTypes = resultTypes
	return &agentType, nil
}

func getTimeMeasurementAgents(db *sql.DB, simID int) (*AgentType, error) {
	var agentType AgentType
	agentType.AgentTypeID = TimeMeasurementsAgentTypeID
	agentType.AgentTypeName = TimeMeasurementAgentTypeName

	agents := make([]Agent, 0)

	rows, err := db.Query(`SELECT id FROM timemeasurements WHERE simulationid=$1 GROUP BY id`, simID)
	if err != nil {
		return nil, err
	}
	for rows.Next() {
		var rank int64
		var agent Agent
		if err := rows.Scan(&rank); err != nil {
			return nil, err
		}
		agent.AgentID = getAgentIDFromRank(rank)
		agents = append(agents, agent)
	}
	agentType.Agents = agents
	return &agentType, nil
}

func getTimeMeasurementResults(db *sql.DB, simid int, agentIDs []string, timeMeasurementTypes []serialization.ResultTypeFilter, start float64, stop float64) (map[float64]AgentResult, error) {
	agentIDs = filterForTimeMeasurementAgents(agentIDs)

	//filter out necessary resulttypes
	resultTypes := make([]string, 0)
	for _, filter := range timeMeasurementTypes {
		if filter.AgentTypeID == TimeMeasurementsAgentTypeID {
			resultTypes = append(resultTypes, filter.ResultTypeName)
		}
	}

	simTimeResultMap := make(map[float64]AgentResult)
	if len(agentIDs) == 0 { // there are zero timeMeasurementAgents, so just return right now
		return simTimeResultMap, nil
	}
	//first get ranks from agentids
	ranks := getRanksFromAgentIDs(agentIDs)
	//get timemeasurements from database
	q := `SELECT simtime, id, timemeasurementtype, duration_ns FROM timemeasurements WHERE simulationid=$1 AND id=ANY($2) AND timemeasurementtype=ANY($3) AND simtime >= $4 AND simtime <= $5`
	rows, err := db.Query(q, simid, pq.Array(ranks), pq.Array(resultTypes), start, stop)
	if err != nil {
		return nil, err
	}
	for rows.Next() {
		var simtime float64
		var timeMeasurementType string
		var id int64
		var durationNs int64
		if err := rows.Scan(&simtime, &id, &timeMeasurementType, &durationNs); err != nil {
			log.Println("Could not scan values:", err)
			return nil, err
		}
		result := Result{ //create a result
			AgentID:        getAgentIDFromRank(id),
			AgentTypeID:    TimeMeasurementsAgentTypeID,
			AgentTypeName:  TimeMeasurementAgentTypeName,
			ResultTypeName: timeMeasurementType,
			Value:          durationNs,
		}
		// check if the result is already in the map
		// if so just append it in the values array
		// otherwise create a new agentResult Object and add it there
		if agentResult, ok := simTimeResultMap[simtime]; ok {
			agentResult.Values = append(agentResult.Values, result)
			simTimeResultMap[simtime] = agentResult //not sure if necessary (Copy or reference?)
		} else {
			results := []Result{result}
			agentResult := AgentResult{
				SimTime: simtime,
				Values:  results,
			}
			simTimeResultMap[simtime] = agentResult
		}
	}
	return simTimeResultMap, nil
}

// identifies timeMeasurementAgents and is used for the agent ID
// make sure it is unique, so no other agentID uses that prefix
const prefix = "tm_"

func filterForTimeMeasurementAgents(agentIDs []string) []string {
	timeMeasurementAgentIDs := make([]string, 0)
	for _, agentID := range agentIDs {
		if strings.HasPrefix(agentID, prefix) {
			timeMeasurementAgentIDs = append(timeMeasurementAgentIDs, agentID)
		}
	}
	return timeMeasurementAgentIDs
}

func getRanksFromAgentIDs(agentIDs []string) []int64 {
	ranks := make([]int64, len(agentIDs))
	for i, agentID := range agentIDs {
		ranks[i] = getRankFromAgentID(agentID)
	}
	return ranks
}

func getAgentIDFromRank(rank int64) string {
	return fmt.Sprintf("%s%d", prefix, rank)
}

func getRankFromAgentID(agentID string) int64 {
	rankStr := strings.Trim(agentID, prefix)
	rank, err := strconv.ParseInt(rankStr, 10, 64)
	if err != nil {
		log.Println("Could not convert timemeasurement string to rank:", err)
		log.Println("Use -1 as agentid instead")
		rank = -1
	}
	return rank
}
