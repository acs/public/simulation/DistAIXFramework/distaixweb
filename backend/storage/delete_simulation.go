// storage package
//
// @author Sonja Happ <sonja.happ@eonerc.rwth-aachen.de>, Jonas Otten
// @copyright 2019, Institute for Automation of Complex Power Systems, EONERC
// @license GNU General Public License (version 3)
//
// DistAIX Web
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

package storage

import (
	"database/sql"
	"fmt"

	"github.com/gocql/gocql"
)

var ErrNoAgentIDs = fmt.Errorf(`No Agents got deleted`)

func DeleteSimulation(cass *gocql.Session, postgres *sql.DB, sidebarDB *sql.DB, simulationID int) error {
	agentIDs, err := getAgentsCassandra(cass, simulationID)
	if err != nil {
		return err
	}
	if len(agentIDs) == 0 {
		return ErrNoAgentIDs
	}
	if err := deleteAgentsCassandra(cass, simulationID, agentIDs); err != nil {
		return err
	}
	if err := deleteAgentIDsCassandra(cass, simulationID); err != nil {
		return err
	}
	if err := deletePostgres(postgres, simulationID); err != nil {
		return err
	}
	return deleteSideBarElements(sidebarDB, simulationID)
}

func deletePostgres(postgres *sql.DB, simulationID int) error {
	tables := []string{"agentmetadata", "resulttypes", "simulationmetadata", "simulations", "timemeasurements"}
	queryTemplate := `DELETE FROM %s WHERE simulationid=$1`
	for _, table := range tables {
		q := fmt.Sprintf(queryTemplate, table)
		if err := postgres.QueryRow(q, simulationID).Scan(); err != nil && err != sql.ErrNoRows {
			return err
		}
	}
	return nil
}

func getAgentsCassandra(cass *gocql.Session, simulationID int) ([]string, error) {
	agents := make([]string, 0)
	q := `SELECT agentid FROM agents WHERE simid=?`
	iter := cass.Query(q, simulationID).Iter()

	var agentID string
	for iter.Scan(&agentID) {
		agents = append(agents, agentID)
	}
	if err := iter.Close(); err != nil {
		return nil, err
	}
	return agents, nil
}

func deleteAgentsCassandra(cass *gocql.Session, simulationID int, agentIDs []string) error {
	q := `DELETE FROM results WHERE simid=? AND agentid IN ?`
	iter := cass.Query(q, simulationID, agentIDs).Iter()
	return iter.Close()
}

func deleteAgentIDsCassandra(cass *gocql.Session, simulationID int) error {
	q := `DELETE FROM agents WHERE simid=?`
	iter := cass.Query(q, simulationID).Iter()
	return iter.Close()
}

func deleteSideBarElements(sidebarDB *sql.DB, simulationID int) error {
	err := sidebarDB.QueryRow("DELETE FROM sidebar WHERE simid=$1", simulationID).Scan()
	if err != nil && err != sql.ErrNoRows {
		return err
	}
	return nil
}
