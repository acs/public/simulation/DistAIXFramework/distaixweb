// storage package
//
// @author Sonja Happ <sonja.happ@eonerc.rwth-aachen.de>, Jonas Otten
// @copyright 2019, Institute for Automation of Complex Power Systems, EONERC
// @license GNU General Public License (version 3)
//
// DistAIX Web
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

package storage

import "database/sql"
import "log"

//GetAgentTypeIDs returns the agenttypeids for a specific simulationid
func GetAgentTypeIDs(db *sql.DB, simid int) ([]int, error) {
	rows, err := db.Query(`SELECT agenttypeid as id FROM agentmetadata WHERE simulationid=$1 GROUP BY agenttypeid`, simid)
	if err != nil {
		log.Println(err)
		return nil, err
	}
	var atids []int
	for rows.Next() {
		var aid int
		if err := rows.Scan(&aid); err != nil {
			return nil, err
		}
		atids = append(atids, aid)
	}
	return atids, nil
}
