// storage package
//
// @author Sonja Happ <sonja.happ@eonerc.rwth-aachen.de>, Jonas Otten
// @copyright 2019, Institute for Automation of Complex Power Systems, EONERC
// @license GNU General Public License (version 3)
//
// DistAIX Web
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

package storage

import (
	"database/sql"
	"fmt"
	"git.rwth-aachen.de/acs/public/DistAIXFramework/distaixweb.git/storage/serialization"
	"log"
	"sort"
	"strconv"
	"strings"

	"github.com/lib/pq"
)

//AgentType is an AgentType for the simulation, containing Agentds
type AgentType struct {
	AgentTypeID   int     `json:"agenttypeid"`
	AgentTypeName string  `json:"agenttypename"`
	Agents        []Agent `json:"agents"`
}

//Agent is a simulation agent, containing an ID and the corresponding metadata
type Agent struct {
	AgentID  string            `json:"agentid"`
	Metadata map[string]string `json:"metadata"`
}

// transfer object
type dbAgent struct {
	AgentTypeID   int
	AgentTypeName string
	AgentID       string
	Metatype      string
	Value         string
}

// GetAgentsFiltered takes agenttypeids and return a map with agenttypeids as key and an array of agentids as value
func GetAgentsFiltered(db *sql.DB, simID int, agentTypeIDs []int) ([]AgentType, error) {
	rows, err := db.Query("SELECT agenttypeid, agentid, metatype, value FROM agentmetadata WHERE simulationid=$1 AND agenttypeid=ANY($2)", simID, pq.Array(agentTypeIDs))
	if err != nil {
		return nil, err
	}
	agentTypes := make(map[int]AgentType)
	for rows.Next() {
		var dba dbAgent
		if err := rows.Scan(&dba.AgentTypeID, &dba.AgentID, &dba.Metatype, &dba.Value); err != nil {
			log.Println("Could not read agents into variables:", err)
		}

		//just add metadata if agentid is already present
		if agenttype, ok := agentTypes[dba.AgentTypeID]; ok {
			//check if the agent is already in the array
			if ag := agentInArray(&agenttype.Agents, dba.AgentID); ag != nil {
				(*ag).Metadata[dba.Metatype] = dba.Value
			} else {
				var ag Agent
				ag.AgentID = dba.AgentID
				ag.Metadata = make(map[string]string)
				ag.Metadata[dba.Metatype] = dba.Value
				agenttype.Agents = append(agenttype.Agents, ag)
				agentTypes[dba.AgentTypeID] = agenttype
			}
		} else {
			var newAgentType AgentType
			//find agenttypename
			agentTypeName := serialization.GetAgentTypeName(dba.AgentTypeID)
			if agentTypeName == nil {
				log.Println("Could not find name for agenttypeid")
			} else {
				newAgentType.AgentTypeName = *agentTypeName
			}

			newAgentType.AgentTypeID = dba.AgentTypeID
			newAgentType.Agents = make([]Agent, 1)

			//create new agent
			var ag Agent
			ag.AgentID = dba.AgentID
			ag.Metadata = make(map[string]string)
			ag.Metadata[dba.Metatype] = dba.Value
			newAgentType.Agents[0] = ag

			//append it
			agentTypes[dba.AgentTypeID] = newAgentType
		}
	}

	//append "agentids" for timemeasurement if required
	for _, v := range agentTypeIDs {
		if v == TimeMeasurementsAgentTypeID {
			timeAgents, err := getTimeMeasurementAgents(db, simID)
			log.Println(timeAgents)
			if err != nil {
				return nil, err
			}
			agentTypes[TimeMeasurementsAgentTypeID] = *timeAgents
			break
		}
	}

	//convert map to array
	retAgentTypes := make([]AgentType, len(agentTypes))
	i := 0
	for _, v := range agentTypes {
		sort.Sort(byAgent(v.Agents))
		retAgentTypes[i] = v
		i++
	}
	return retAgentTypes, nil
}

//sort
type byAgent []Agent

func (ar byAgent) Len() int {
	return len(ar)
}

func (ar byAgent) Swap(i, j int) {
	ar[i], ar[j] = ar[j], ar[i]
}

func (ar byAgent) Less(i, j int) bool {
	agentIDa, erra := strconv.Atoi(ar[i].AgentID)
	agentIDb, errb := strconv.Atoi(ar[j].AgentID)
	//if both are parseable, sort numerically
	if errb == nil && erra == nil {
		return agentIDa < agentIDb
	}
	//for cables
	bLess, err := parseCables(ar[i].AgentID, ar[j].AgentID)
	if err == nil {
		return bLess
	}
	//default case, do string comparison
	return ar[i].AgentID < ar[j].AgentID
}

func parseCables(a, b string) (bool, error) {
	if !strings.Contains(a, "_") || !strings.Contains(b, "_") {
		return false, fmt.Errorf("Not both strings contain an underscore")
	}
	//split into parts
	agentaParts := strings.Split(a, "_")
	agentbParts := strings.Split(b, "_")
	if len(agentaParts) != 2 || len(agentbParts) != 2 {
		return false, fmt.Errorf("one of the strings does contain more or less than one underscores")
	}

	//parse agent a to number
	agentaFirst, err := strconv.Atoi(agentaParts[0])
	if err != nil {
		return false, err
	}
	agentaSecond, err := strconv.Atoi(agentaParts[1])
	if err != nil {
		return false, err
	}
	//parse agent b to number
	agentbFirst, err := strconv.Atoi(agentbParts[0])
	if err != nil {
		return false, err
	}
	agentbSecond, err := strconv.Atoi(agentbParts[1])
	if err != nil {
		return false, err
	}

	//compare
	//if first part is not equal, use that
	if agentaFirst != agentbFirst {
		return agentaFirst < agentbFirst, nil
	}
	//otherwise the second
	return agentaSecond < agentbSecond, nil

}

func agentInArray(agents *[]Agent, agentID string) *Agent {
	for _, a := range *agents {
		if a.AgentID == agentID {
			return &a
		}
	}
	return nil
}
