// storage package
//
// @author Sonja Happ <sonja.happ@eonerc.rwth-aachen.de>, Jonas Otten
// @copyright 2019, Institute for Automation of Complex Power Systems, EONERC
// @license GNU General Public License (version 3)
//
// DistAIX Web
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

package storage

import (
	"database/sql"
	"git.rwth-aachen.de/acs/public/DistAIXFramework/distaixweb.git/storage/serialization"
	"log"
	"sort"

	"container/list"

	"github.com/gocql/gocql"
)

type AgentResult struct {
	SimTime float64  `json:"simTime"`
	Values  []Result `json:"values"`
}

type Result struct {
	AgentID        string      `json:"agentID"`
	AgentTypeName  string      `json:"agentTypeName"`
	AgentTypeID    int         `json:"agentTypeID"`
	ResultTypeName string      `json:"resultTypeName"`
	Value          interface{} `json:"value"`
}

// GetResultsNoLimits returns the 30 latest results
func GetResultsNoLimits(db *sql.DB, session *gocql.Session, simID int, agentIDs []string, filterResultTypes []serialization.ResultTypeFilter) ([]AgentResult, error) {
	//get a random "real" agentID
	//needed because it could be a timemeasurement
	agentID, err := getAgentIDforSim(session, simID)
	if err != nil {
		return nil, err
	}
	//get last 30 keys
	const resultCount = 30
	l := list.New()
	iter := session.Query(`SELECT simtime FROM results WHERE simid=? AND agentid=?`, simID, agentID).Iter()
	var simtime float64
	for iter.Scan(&simtime) {
		l.PushBack(simtime)
		//if size is greater that the desired count, remove the oldest element
		if l.Len() > resultCount {
			front := l.Front()
			l.Remove(front)
		}
	}
	//get start & stop
	if l.Front() == nil || l.Back() == nil {
		a := make([]AgentResult, 0)
		return a, nil
	}
	start := l.Front().Value.(float64)
	stop := l.Back().Value.(float64)
	return GetResults(db, session, simID, agentIDs, start, stop, filterResultTypes, -1)
}

func getAgentIDforSim(session *gocql.Session, simid int) (string, error) {
	var agentID string
	err := session.Query(`SELECT agentid FROM agents WHERE simid=? LIMIT 1`, simid).Scan(&agentID)
	return agentID, err
}

// GetResults return the results with specified start & stop
func GetResults(db *sql.DB, session *gocql.Session, simID int, agentIDs []string, start float64, stop float64, filterResultTypes []serialization.ResultTypeFilter, aggregateCount int) ([]AgentResult, error) {
	q := `SELECT agentid, agenttypeid, simtime, result FROM results WHERE simid=? AND agentid IN ? AND simtime >= ? AND simtime <= ?`
	iter := session.Query(q, simID, agentIDs, start, stop).Iter()

	var dbAgentTypeID int
	var dbAgentID string
	var dbSimTime float64
	var dbSerialized []byte

	// create  map to group results by simTime
	m := make(map[float64]AgentResult)

	for iter.Scan(&dbAgentID, &dbAgentTypeID, &dbSimTime, &dbSerialized) {
		//try to get agentType name
		var name string
		namePtr := serialization.GetAgentTypeName(dbAgentTypeID)
		if namePtr != nil {
			name = *namePtr
		}

		var resultValues map[string]interface{}
		var err error

		// if we got specific resulttypes, only get these
		// otherwise get all available resulttypes
		if filterResultTypes != nil {
			_, resultValues, err = serialization.DeserializeToMapWithResulttypes(dbAgentTypeID, dbSerialized, filterResultTypes)
		} else {
			resultValues, err = serialization.DeserializeToMap(dbAgentTypeID, dbSerialized)
		}

		if err != nil {
			return nil, err
		}
		// could not deserialize
		if resultValues == nil {
			log.Printf("Could not deserialize AgentTypeID %d %s", dbAgentTypeID, name)
			continue
		}

		agentResult, ok := m[dbSimTime]
		// if it already exists, just add a new value
		if ok {
			m[dbSimTime] = addToAgentResult(agentResult, dbAgentID, dbAgentTypeID, resultValues)
		} else {
			values := make([]Result, 0)
			ar := AgentResult{SimTime: dbSimTime, Values: values}
			m[dbSimTime] = addToAgentResult(ar, dbAgentID, dbAgentTypeID, resultValues)
		}
	}

	if err := iter.Close(); err != nil && err != gocql.ErrNotFound {
		log.Print(err)
		return nil, err
	}

	//****************************ATTENTION******************************//
	//*                                                                 *//
	//*         Since we needed a solution for timemeasurements         *//
	//*	        we decided to treat them as results so they can	        *//
	//*	        get easily examined in the frontend. That means	        *//
	//*	        we need to merge them with the real reasults,           *//
	//*	        that happens here.                                      *//
	//*******************************************************************//

	//get all timeMeasurementResults which got requested
	timeMeasurementResults, err := getTimeMeasurementResults(db, simID, agentIDs, filterResultTypes, start, stop)
	if err != nil {
		return nil, err
	}

	//iterate through all timeMeasurementResults
	for simtime, timeMeasurementResult := range timeMeasurementResults {
		if agentResult, ok := m[simtime]; ok {
			agentResult.Values = append(agentResult.Values, timeMeasurementResult.Values...) //already an agentResult available, just add the values
			m[simtime] = agentResult                                                         //necessary?
		} else {
			m[simtime] = timeMeasurementResult //no agentResult available for the simtime, add it here
		}
	}

	// convert from map to array
	ret := make([]AgentResult, 0)
	for k := range m {
		ret = append(ret, m[k])
	}

	sort.Sort(bySimTime(ret))
	return ret, nil
}

type resultCombination struct {
	agentID        string
	resultTypeName string
}

// add a value to a agent result and return it
func addToAgentResult(ar AgentResult, agentID string, agentTypeID int, values map[string]interface{}) AgentResult {
	//only add sth if there are actual values
	//check if theres a value in the map
	if len(values) == 0 {
		return ar
	}

	var name string
	if agentTypeName := serialization.GetAgentTypeName(agentTypeID); agentTypeName != nil {
		name = *agentTypeName
	}

	for k := range values {
		r := Result{
			AgentID:        agentID,
			AgentTypeName:  name,
			AgentTypeID:    agentTypeID,
			ResultTypeName: k,
			Value:          values[k],
		}

		ar.Values = append(ar.Values, r)
	}
	return ar
}

//sort
type bySimTime []AgentResult

func (ar bySimTime) Len() int {
	return len(ar)
}

func (ar bySimTime) Swap(i, j int) {
	ar[i], ar[j] = ar[j], ar[i]
}

func (ar bySimTime) Less(i, j int) bool {
	return ar[i].SimTime < ar[j].SimTime
}
