// storage package
//
// @author Sonja Happ <sonja.happ@eonerc.rwth-aachen.de>, Jonas Otten
// @copyright 2019, Institute for Automation of Complex Power Systems, EONERC
// @license GNU General Public License (version 3)
//
// DistAIX Web
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

package storage

import (
	"database/sql"
	"fmt"
	"log"

	// necessary to open a postgres database
	_ "github.com/lib/pq"
)

// Open a new database based on the flags parameters
func Open(postgresUser string, postgresPassword string, postgresHost string, postgresPort string, postgresDB string) (*sql.DB, error) {
	log.Println("Connecting to DB", postgresDB, "on host", postgresHost, ":", postgresPort, "(", postgresUser, ",", postgresPassword, ")")
	dbstr := fmt.Sprintf(
		"postgres://%s:%s@%s:%s/%s?sslmode=disable",
		postgresUser, postgresPassword, postgresHost, postgresPort, postgresDB)
	return sql.Open("postgres", dbstr)
}

// SIDEBARDB defines the database which should be used for the sidebar
const SIDEBARDB = "sidebar"

// OpenSidebarConn opens the sidebar database, creates it if it doesn't exist and
// creates the table if it doesn't exist
func OpenSidebarConn(postgresUser string, postgresPassword string, postgresHost string, postgresPort string, postgresDB string) (*sql.DB, error) {
	//temporary connection to create DB
	conn, err := Open(postgresUser, postgresPassword, postgresHost, postgresPort, postgresDB)
	if err != nil {
		return nil, err
	}
	if err := createDBIfNotExist(conn, SIDEBARDB); err != nil {
		return nil, err
	}
	if err := conn.Close(); err != nil {
		return nil, err
	}

	db, err := Open(postgresUser, postgresPassword, postgresHost, postgresPort, SIDEBARDB)
	if err != nil {
		return nil, err
	}

	if err := createSidebarTableIfNotExist(db); err != nil {
		return nil, err
	}
	return db, nil
}

func createSidebarTableIfNotExist(db *sql.DB) error {
	err := db.QueryRow("CREATE TABLE IF NOT EXISTS sidebar(sidebarid serial, name text PRIMARY KEY, simid int, resulttypes_left text, resulttypes_right text, agentids text)").Scan()
	if err == sql.ErrNoRows {
		return nil
	}
	return err
}

func createDBIfNotExist(db *sql.DB, name string) error {
	var count int64
	err := db.QueryRow("SELECT COUNT(1) FROM pg_database WHERE datname = $1", name).Scan(&count)
	//return if database exists or error happend
	if err != nil || count == 1 {
		if err == nil {
			err = db.Close()
		}
		return err
	}
	err = db.QueryRow(fmt.Sprintf("CREATE DATABASE %s", name)).Scan()
	if err != nil && err != sql.ErrNoRows {
		return err
	}
	return nil
}
