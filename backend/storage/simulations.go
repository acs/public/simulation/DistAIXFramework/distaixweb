// storage package
//
// @author Sonja Happ <sonja.happ@eonerc.rwth-aachen.de>, Jonas Otten
// @copyright 2019, Institute for Automation of Complex Power Systems, EONERC
// @license GNU General Public License (version 3)
//
// DistAIX Web
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

package storage

import (
	"database/sql"
	"sort"
)

type Simulation struct {
	SimulationID int                  `json:"simulationid"`
	Start        string               `json:"start"`
	Stop         string               `json:"stop"`
	Metadata     []SimulationMetadata `json:"metadata"`
}

type SimulationMetadata struct {
	Metatype string `json:"metatype"`
	Key      string `json:"key"`
	Value    string `json:"value"`
	ID       int    `json:"id"`
}

type simulationRow struct {
	SimulationID int
	Start        string
	Stop         string
	Metatype     string
	Key          string
	Value        string
	ID           int
}

func GetSimulations(db *sql.DB) ([]Simulation, error) {
	rows, err := db.Query("SELECT s.simulationid, to_char(s.start_ts,'HH24:MI:SS DD-MM-YYYY'), to_char(s.stop_ts,'HH24:MI:SS DD-MM-YYYY'), sm.metatype, sm.id, sm.key, sm.value FROM simulations s JOIN simulationmetadata sm ON s.simulationid = sm.simulationid ORDER BY s.simulationid ASC, sm.metatype ASC, sm.id ASC")
	if err != nil {
		return nil, err
	}
	simulations := make(map[int]Simulation)

	for rows.Next() {
		var s simulationRow
		var start, stop sql.NullString
		if err := rows.Scan(&s.SimulationID, &start, &stop, &s.Metatype, &s.ID, &s.Key, &s.Value); err != nil {
			return nil, err
		}
		//make sure stop column isn't null
		if start.Valid {
			s.Start = start.String
		}
		if stop.Valid {
			s.Stop = stop.String
		}
		// create a simulationmetadata struct because it is needed later
		var sm SimulationMetadata
		sm.Key = s.Key
		sm.Metatype = s.Metatype
		sm.Value = s.Value
		sm.ID = s.ID

		//check if already in map
		if sim, ok := simulations[s.SimulationID]; ok {
			sim.Metadata = append(sim.Metadata, sm)
			simulations[s.SimulationID] = sim
		} else {
			var sim Simulation
			sim.SimulationID = s.SimulationID
			sim.Start = s.Start
			sim.Stop = s.Stop
			sim.Metadata = make([]SimulationMetadata, 1)
			sim.Metadata[0] = sm
			simulations[s.SimulationID] = sim
		}
	}
	//convert map to array
	simReturn := make([]Simulation, 0)
	for _, v := range simulations {
		simReturn = append(simReturn, v)
	}
	sort.Sort(bySimID(simReturn))
	return simReturn, nil
}

type bySimID []Simulation

func (a bySimID) Len() int           { return len(a) }
func (a bySimID) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a bySimID) Less(i, j int) bool { return a[i].SimulationID < a[j].SimulationID }
