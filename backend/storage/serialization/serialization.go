package serialization

import (
	"log"
	"reflect"
	"sort"
	"strings"

	"github.com/golang/protobuf/proto"
)

//DeserializeToMap an serialized agent back to a map of struct keys + values
func DeserializeToMap(agentTypeID int, serialized []byte) (map[string]interface{}, error) {
	val, ok := agentDefinitions[agentTypeID]
	if !ok {
		return nil, nil //errors.New("could not find type to deserialize")
	}
	if err := proto.Unmarshal(serialized, val); err != nil {
		return nil, err
	}
	m := make(map[string]interface{})
	s := reflect.ValueOf(val).Elem()
	typeOfT := s.Type()
	for i := 0; i < s.NumField(); i++ {
		f := s.Field(i)
		name := typeOfT.Field(i).Tag.Get("json")
		name = strings.SplitN(name, ",", 2)[0]
		m[name] = f.Interface()
	}
	return m, nil
}

//ResultTypeFilter is a filter for a specific result type
type ResultTypeFilter struct {
	AgentTypeID    int
	ResultTypeName string
}

//DeserializeToMapWithResulttypes deserializes serialized bytes into a map, mapping from resulttype to value
func DeserializeToMapWithResulttypes(agentTypeID int, serialized []byte, filters []ResultTypeFilter) (string, map[string]interface{}, error) {
	val, ok := agentDefinitions[agentTypeID]
	if !ok {
		return "", nil, nil //errors.New("could not find type to deserialize")
	}
	if err := proto.Unmarshal(serialized, val); err != nil {
		return "", nil, err
	}
	m := make(map[string]interface{})
	s := reflect.ValueOf(val).Elem()
	typeOfT := s.Type()
	//iterate through all fields and just take the wanted ones
	for i := 0; i < s.NumField(); i++ {
		f := s.Field(i)
		jsonname := typeOfT.Field(i).Tag.Get("json")
		resultTypeName := strings.SplitN(jsonname, ",", 2)[0]
		for _, filter := range filters {

			if filter.AgentTypeID == agentTypeID && filter.ResultTypeName == resultTypeName { // if resulttype is wanted and it's not set to the null value, add it
				m[resultTypeName] = f.Interface()
			}
		}

	}
	return reflect.TypeOf(val).Elem().Name(), m, nil
}

//AgentType contains a specific agentID, a agent Type and an ResultTypes Array
//used to send back to the client
type AgentType struct {
	ID          int      `json:"id"`
	Type        string   `json:"type"`
	ResultTypes []string `json:"resulttypes"`
}

//GetResultTypes returns the AgentTypes based on the agentTypeIDs
func GetResultTypes(types []int) []AgentType {
	var ret []AgentType
	for _, agentTypeID := range types {
		agentDef, ok := agentDefinitions[agentTypeID]
		if !ok {
			log.Printf("Could not find agent type %d for deserialization", agentTypeID)
			log.Println("Is it registered?")
			continue
		}
		agentType := reflect.TypeOf(agentDef).Elem()
		var resulttypes []string
		// append resulttypes to array
		for i := 0; i < agentType.NumField(); i++ {
			name := agentType.Field(i).Tag.Get("json")
			name = strings.SplitN(name, ",", 2)[0]
			resulttypes = append(resulttypes, name)
		}
		a := AgentType{ID: agentTypeID, Type: agentType.Name(), ResultTypes: resulttypes}
		ret = append(ret, a)
	}
	sort.Sort(ByID(ret))
	return ret
}

//GetAgentTypeName takes an agentTypeID and returns the name, if not found nil
func GetAgentTypeName(id int) *string {
	var agent *proto.Message
	for k := range agentDefinitions {
		if k == id {
			t := agentDefinitions[k]
			agent = &t
			break
		}
	}
	if agent == nil {
		return nil
	}
	name := reflect.TypeOf(*agent).Elem().Name()
	return &name
}

//ByID sorts AgentTypes by ID
type ByID []AgentType

func (as ByID) Len() int {
	return len(as)
}
func (as ByID) Swap(i, j int) {
	as[i], as[j] = as[j], as[i]
}
func (as ByID) Less(i, j int) bool {
	return as[i].ID < as[j].ID
}
