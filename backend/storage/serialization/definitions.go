package serialization

import (
	"git.rwth-aachen.de/acs/public/DistAIXFramework/distaixweb.git/agents_protobuf"

	"github.com/golang/protobuf/proto"
)

//one of the most important files
//here you define the mapping from agenttypeid to the corresponding struct
var agentDefinitions = map[int]proto.Message{
	-3: &agentsProtobuf.MRResult{},
	-2: &agentsProtobuf.DFResult{},
	0:  &agentsProtobuf.LoadResult{},
	1:  &agentsProtobuf.PvResult{},
	2:  &agentsProtobuf.BatteryResult{},
	3:  &agentsProtobuf.TransformerResult{},
	4:  &agentsProtobuf.SlackResult{},
	5:  &agentsProtobuf.NodeResult{},
	6:  &agentsProtobuf.ChpResult{},
	7:  &agentsProtobuf.ElectricVehicleResult{},
	8:  &agentsProtobuf.CompensatorResult{},
	9:  &agentsProtobuf.WecResult{},
	99: &agentsProtobuf.CableResult{},
	11: &agentsProtobuf.HPResult{},
	17: &agentsProtobuf.BiofuelResult{},
}
