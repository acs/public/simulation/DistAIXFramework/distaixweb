# post-nodestatus.sh script
#
# @author Sonja Happ <sonja.happ@eonerc.rwth-aachen.de>, Jonas Otten
# @copyright 2019, Institute for Automation of Complex Power Systems, EONERC
# @license GNU General Public License (version 3)
#
# DistAIX Web
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
################################################################################

#!/bin/bash
API=https://distaixweb.k8s.eonerc.rwth-aachen.de

available=`df -h | awk '$NF == "/" { print $4 }'`
used_percentage=`df -h | awk '$NF == "/" { print $5 }'`

echo "Available Space: $available ($used_percentage used)" > status.txt
curl --insecure -X POST --data @status.txt  $API/api/v1/nodestatus/`hostname`
rm status.txt

