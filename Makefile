# Makefile
#
# @author Sonja Happ <sonja.happ@eonerc.rwth-aachen.de>, Jonas Otten
# @copyright 2019, Institute for Automation of Complex Power Systems, EONERC
# @license GNU General Public License (version 3)
#
# DistAIX Web
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
################################################################################

export GOPATH=$(CURDIR)/backend
export PATH=$PATH:$(CURDIR)/backend/bin:$GOROOT/bin

#paths to executables
GO_EXE=$(shell which go)
RM_EXE=$(shell which rm)
MKDIR_EXE=$(shell which mkdir)
MAKE_EXE=$(shell which make)

# path to folder where proto files of DistAIX are stored
PROTOC_INCLUDE=$(realpath ../distaix/proto)

# path to folder where agent protobuf go files shall be stored
PROTOC_GO_OUT=backend/agents_protobuf

PROTO_FILE_NAMES=$(foreach file,$(shell ls $(PROTOC_INCLUDE)/*.proto), $(shell realpath $(file)))

all: protobuf

protobuf: protoc-gen-go $(PROTOC_GO_OUT)

protoc-gen-go:
	$(GO_EXE) get -u github.com/golang/protobuf/protoc-gen-go

$(PROTOC_GO_OUT): $(PROTO_FILE_NAMES)
	$(MKDIR_EXE) $(PROTOC_GO_OUT)
	$(MAKE_EXE) -C $(PROTOC_INCLUDE)/ go

clean:
	$(RM_EXE) -rf ./distaixweb $(PROTOC_GO_OUT)

.PHONY: clean protobuf protoc
