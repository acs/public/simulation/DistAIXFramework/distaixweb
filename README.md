# DistAIX Web

[![pipeline status](https://git.rwth-aachen.de/acs/research/swarmgrid/swarmweb/badges/master/pipeline.svg)](https://git.rwth-aachen.de/acs/research/swarmgrid/swarmweb/commits/master)

DistAIXweb consists of two components:

- Frontend
- Backend

The backend provides a REST-style interface with methods to view the metadata and get the results, which is used by the frontend to offer an easy way to access simulation results.
The backend server application starts the frontend server automatically.


## Further documentation
| Document                                      |
|-----------------------------------------------|
| [Database structure](doc/README_databases.md) |
| [Frontend internals](doc/README_frontend.md)  |
| [Backend internals](doc/README_backend.md)    |
| [Deployment](doc/README_deploy.md)            |

## Copyright

2022, Institute for Automation of Complex Power Systems, EONERC  

## License

This project is released under the terms of the [GPL version 3](COPYING.md).

```
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
```

For other licensing options please consult [Prof. Antonello Monti](mailto:amonti@eonerc.rwth-aachen.de).

## Contact

- Sonja Happ <sonja.happ@eonerc.rwth-aachen.de>

[Institute for Automation of Complex Power Systems (ACS)](http://www.acs.eonerc.rwth-aachen.de)  
[EON Energy Research Center (EONERC)](http://www.eonerc.rwth-aachen.de)  
[RWTH University Aachen, Germany](http://www.rwth-aachen.de)  
