# Frontend documentation

The frontend is written with React and has only one (runtime) dependency: recharts
It uses `babel` and `webpack` for building/transpiling all components.

## Local building and deployment

Building is done by webpack, which transpiles all jsx (which use the `.js` suffix in this project) and css files into one `bundle.js` file.

To install dependencies, run:
```
npm install
```


For development, you can use the following to build and watch all files
```
npm run build
```

The frontend development server is accessible on `localhost:8080` by default and expects the backend on `localhost:4000`.
To change the default settings you need to modify the `webpack.config.js` file.

If you only want to build once and minimize the bundle size, use

```
npm run build-prod
```
It generates `public/dist/bundle.js`

## Structure

React makes it easy to put all components in different modules.

The main component is `app/app.js`, from there it includes all necessary components.

Let's look at the directory tree with a description:
```
.
├── app.js (Main Component)
├── components (Components for the main app)
│   ├── AggregateSelector (used in Prepared Results)
│   ├── creator (Components for the graph creator/setup)
│   │   ├── components
│   │   │   ├── AgentCableSelector.js (Select agents/cables)
│   │   │   ├── FilterSelector.js (Select agenttypes)
│   │   │   ├── GraphPreview.js (Preview for the graph, or direct download)
│   │   │   ├── ResultTypeSelector.js (Select resulttypes)
│   │   │   └── SimulatorSelector.js (Select a simulation)
│   │   └── GraphCreator.js (Graph Setup)
│   ├── DeleteGraph.js (Delete a Graph)
│   ├── DeleteSimulation.js (Delete a Simulation)
│   ├── DistAIXGraph.js (Graph)
│   └── DistAIXSidebar.js (Sidebar components)
│   ├── GraphControl.js (Graph Control, which includes all control elements)
│   ├── GraphView.js (Graph view, uses DistAIXGraph for Visulation and GraphControl for contol elements)
│   ├── NodeStatus.js (Show the status of the nodes)
│   ├── PreparedResults.js (Prepared Results)
│   ├── ResultTypeSelector.js (used in Prepared Results)
│   ├── requests (class for requests, definitly needs a cleaning/refactoring)
│   │   ├── createURL.js
│   │   ├── createURLQuery.js
│   │   ├── loadData.js
│   │   └── requests.js (request library, which has a simple interface to do get/post/deletes)
│   ├── SimulationSelector.js (used in Prepared Results)
```

This is mirrored by the app/css folder, which includes one css file for every js file, to make sure the styling is as modular as possible.

*But*: Take care of class/id collisions between modules in the css files.
They get all merged into one file, so you might run into some issues if you don't.

### Common/Known Issues

#### 500 Errors
It might happen that there are some `500` errors when using the frontend.
That could happen due to a running simulation, which leads to a lot of pressure on the cassandra nodes, which are not able to respond within the timeout to the requests issued by the backend.

**Possible solution:** Scaling up the cassandra cluster