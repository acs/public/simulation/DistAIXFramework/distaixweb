# Backend documentation


## Local building and deployment
To build the backend run in the directory `backend`:

1. `go mod tidy`
2. `go build server.go`

Go 1.11 or later is required to work with Go Modules concept.

To start the backend run:

```
go run server.go <options>
```

For a list of the available options and their default values run:

```
go run server.go --help
```

Be default, the backend will be deployed locally on `localhost:4000`

## Structure

The backend is modularised and has the following file tree:

```
backend
├── server.go (main file)
├── agents_proto_files (proto files to serialize/ deserialize data)
├── agents_protobuf (auto generated files by protoc)
|   └──...
├── api (API documentation yaml and Makefile to generate html using swagger-codegen)
├── flags (flag parser)
│   └── flags.go
├── handlers (handlers for the web endpoints, uses the storage package to get 
│   │         actual data from the databases)
│   ├── agents
│   │   └── agents.go
│   ├── agenttypes
│   │   └── agenttypes.go
│   ├── aggregatePerAgent (prepared results)
│   │   ├── aggregatePerAgentCSV.go
│   │   └── aggregatePerAgent.go
│   ├── aggregatePerTimeStep (prepared results)
│   │   ├── aggregatePerTimestepCSV.go
│   │   └── aggregatePerTimestep.go
│   ├── nodestatus
│   │   └── nodestatus.go
│   ├── results
│   │   ├── handlers.go
│   │   ├── noLimits.go
│   │   └── params.go
│   ├── sidebar
│   │   └── sidebar.go
│   └── simulations
│       └── simulations.go
├── storage (abstraction layer to get actual data from postgres or cassandra,
│   │         and deserialize it)
│   ├── agents.go
│   ├── agenttypes.go
│   ├── delete_simulation.go
│   ├── results.go
│   ├── resulttypes.go
│   ├── serialization
│   │   ├── definitions.go (VERY IMPORTANT: mapping from agenttypeIDs to
│   │   │     actual protobuf structs, if its not registered here it wont
│   │   │     get serialized)
│   │   └── serialization.go (methods to deserialize everything)
│   ├── sidebar.go
│   ├── simulations.go
│   ├── storage.go
│   ├── timemeasurements.go
│   └── times.go
├── web
│   ├── context.go (wraps a cassandra session, postgres in order to get used
│   │              by the web methods)
│   └── helpers.go (helper methods to write for example JSON)
└── web-config
    ├── results.go (web methods for results, own file because they're a lot)
    └── web.go (web method initialization and registration)

```

If you want update or create something, it is recommended to start at `backend/web-config/web.go`.

Each endpoint is registered in `web-config`, and calls a handler in the `handler` package.
That handler then uses the storage package to get actual data
```
web-config
    |
    V
handlers
    |
    V
storage
```

## Common Issues
### Unregistered AgentTypes
Make sure you add an entry at `backend/storage/serialization/definitions.go`

### Unknown data types
You might get an error message likes this:
```
2018/03/16 15:05:22 Could not find corresponding type to result: int32
2018/03/16 15:05:22 Could not find resulttype in result map for variance calculation...something is wrong here
```

The backend does not know how to handle a specific datatype, in this case an `int32`.

In the backend there are 3 different files where an explicit datatype conversion is necessary:

1. `backend/handlers/aggregatePerAgent/aggregatePerAgent.go:addResultToMap` add a case to the switch statement, if there is nothing found this result will be skipped
2. `backend/handlers/aggregatePerTimeStep/aggregatePerTimestep.go:addResultToMap` add a case to the switch statement, if there is nothing found this result will be skipped
3. `backend/handlers/results/results.go:HandleResultWithResulttypesCSV` add a case to the switch statement, if there is nothing found it will use fmt.Sprint, which should be fine in most cases