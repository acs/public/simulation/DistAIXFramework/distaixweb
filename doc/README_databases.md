# Database structure

There are 3 databases used by DistAIXweb
1. Meta-data DB on PostgreSQL: storing meta-data of simulations, typically named `distaix`
2. Sidebar DB on PostgreSQL to store the sidebar elements for the frontend
3. Time Series DB on Cassandra to store simulation results per time step, typically named `distaix`

These three databases are all used by the backend. Nr 1. and Nr 3. are filled by the DistAIX simulator.

## Setup new database clusters
- [Postgres](https://wiki.ubuntuusers.de/PostgreSQL/): stop after setting the admin password
- [Cassandra](https://www.digitalocean.com/community/tutorials/how-to-install-cassandra-and-run-a-single-node-cluster-on-ubuntu-14-04): we use cassandra version 3.11

## Tables

The database tables are created by the simulator DistAIX or the backend (sidebar DB).

### Meta-data DB on PostgreSQL (`distaix`)
|Table| Columns | Description                                                                                                                                                                                                                                                  | 
|-----|-----------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|simulations| simulationid, start time, stop time | Simulation IDs are auto generated                                                                                                                                                                                                                            |
|simulationmetadata| simulationid, id (which is often used for the rank), metatype, key (e.g. agents), value (e.g. 311) | Meta-data for simulations, such as configurations.                                                                                                                                                                                                           |
|agentmetadata| simulationid, metatype (e.g. Vnom), agentid (e.g. 4712), agenttypeid (e.g. 0), value (e.g. 400) | Meta-data for agents.                                                                                                                                                                                                                                        |
|resulttypes| resulttype, simulationid, unit, description | Specification of resulttypes for a an agenttypeid.                                                                                                                                                                                                           |
|timemeasurements| simulationid, timemeasurementtype, id, duration_ns, simtime  | Timemeasurements, which are used for internal measurements, e.g. synctime of agents, ticktime.                                                                                                                                                               |
|timemeasurementtypes| | Used to save a description for timemeasurementtypes. Referenced by timemeasurements to make sure all timemeasuremnttypes are correctly used. Currently only implemented in the dbconnector, therefore it's not possible to show these descriptions right now |

### Sidebar DB on PostgreSQL (only used by the backend, not by the simulator DistAIX)
|Table| Columns | Description                                                                                                                                      |
|-----|----------|--------------------------------------------------------------------------------------------------------------------------------------------------|
|sidebar| name, simid, resulttypes_left (left y-axis), resulttypes_right (right y-axis), agentids, sidebarid| Used to save sidebar items (graphs).  |

### Time Series DB on Cassandra (`distaix`)
|Table| Columns                                                                    | Description                                                                                                                                                                |
|-----|----------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|agents| simid, agentid                                                             | Due to the primary key of the results table, this table is necessary to delete simulation only with its simulationid. This table just saves all agentids for a simulation. |
|results| agentid, simid, simtime, agenttypeid, result  | Saves serialized results. Results are binary blobs serialized with Protocol Buffers                                                                                        |

## Configuration of PostgreSQL database node

- Regular postgres instance, no fancy stuff to take care of here.
- DistAIX and DistAIXweb are tested with PostgeSQL 9.5 and 10
- If you are using a VM, you may want to place the file `post-nodestatus.sh` in the home directory
  - It can be used in a cron job to automatically report the free memory of the node to the backend and display it in the frontend
  - You may need to modify the endpoints used in this file to fit your setup
  
Some important/noteworthy options in `/etc/postgresql/9.5/main/postgresql.conf`

```
listen_addresses = '*'
```

Options to change in `/etc/postgresql/9.5/main/pga_hba.conf`:

```
host    all all 0.0.0.0/0   md5 # allow network connections from everywhere
```

## Configuration of Cassandra database

- Regular cassandra  instance
- DistAIX and DistAIXweb are tested with Cassandra 3.11
- Similar to the PostgreSQL node, you may want to use the `post-nodestatus.sh` script in a cron job (see above)

Important lines for configuration at `/etc/cassandra/cassandra.yaml`:

| line                                     | description                                                                                                         |
|------------------------------------------|---------------------------------------------------------------------------------------------------------------------|
| `cluster_name: 'your-cluster-name'`      | Change the name of your cassandra cluster, has to be the same on all cassandra nodes if you are using multiple node |
| `rpc_address: 0.0.0.0`                   | accept all IPs                                                                                                      |
| `broadcast_rpc_address: aaa.bbb.ccc.ddd` | tell other machines that this is my address (has to be externally reachable IP/ floating IP)                        |
| `auto_snapshot: false`                   | Disabled, because otherwise basically nothing will ever get deleted.                                                |

If you want to use a cluster of cassandra nodes (multiple DB nodes), you have to modify the following in each node

| line                                     | description                                                                                                             |
|------------------------------------------|-------------------------------------------------------------------------------------------------------------------------|
| `seeds: aaa.bbb.ccc.ddd,www.xxx.yyy.zzz` | Machines used for the cluster. One of the IP adresses, needs to be an address of a physical adapter (not a floating IP) |
| `listen_address: aaa.bbb.ccc.ddd`       | Needs to be an IP address of a physical adapter                                                                         |

After changing the configuration file,  stop Cassandra with
```
sudo systemctl stop cassandra
```
and delete EVERYTHING in the folder `/var/lib/cassandra/` but not the folder itself. After that, restart Cassandra with
```
sudo systemctl start cassandra
```

## Deleting content of all DBs / Clean up
To remove all data from all DBs the script ```delete-all-dbs.sh``` in the top level of the repository is provided.
Depending on your setup, you may need to modify the addresses, DB and user names of the PostgreSQL and Cassandra DB machines.